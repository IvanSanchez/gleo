import Geometry from "../../src/geometry/Geometry.mjs";
import Platina from "../../src/Platina.mjs";
import cartesian from "../../src/crs/cartesian.mjs";
import RadarSweep from "../../src/symbols/RadarSweep.mjs";
import { setFactory } from "../../src/geometry/DefaultGeometry.mjs";

setFactory(function cartesianize(coords) {
	return new Geometry(cartesian, coords);
});

function spawnPlatina() {
	const context = contextFactory(200, 200, { preserveDrawingBuffer: true });
	return [
		new Platina(context, {
			crs: cartesian,
			center: [0, 0],
			scale: 2,
			preserveDrawingBuffer: true,
			backgroundColour: "white",
		}),
		context,
	];
}

describe("RadarSweep", function () {
	it("Single", async function () {
		const [platina, context] = spawnPlatina();

		new RadarSweep([-50, -70], { radius: 60, colour: "red", speed: 0 }).addTo(
			platina
		);
		await platina.once("render");

		await expectPixelmatch(context, "spec/07-radars/radars-single-frame1", 2200, 0);
	});
});
