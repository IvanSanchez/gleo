import Geometry from "../../src/geometry/Geometry.mjs";
import Platina from "../../src/Platina.mjs";
import wirify from "../../src/symboldecorators/wirify.mjs";
import Stroke from "../../src/symbols/Stroke.mjs";
import cartesian from "../../src/crs/cartesian.mjs";

const WireStroke = wirify(Stroke);

function spawnPlatina() {
	const context = contextFactory(256, 256, { preserveDrawingBuffer: true });
	return [
		new Platina(context, {
			crs: cartesian,
			center: new Geometry(cartesian, [0, 0]),
			scale: 0.01,
			preserveDrawingBuffer: true,
		}),
		context,
	];
}

const geom = new Geometry(cartesian, [
	[0.2, 0.1],
	[0.6, 0.7],
	[0.9, -0.3],
	[-0.5, -0.22],
	[0.1, -0.7],
	[0.8, -0.72],
	[0.9, -1.2],
]);

describe("Wirestroke", function () {
	it("Miter", async function () {
		const [platina, context] = spawnPlatina();

		new WireStroke(geom, {
			colour: "black",
			width: 20,
			joins: WireStroke.MITER,
			centerline: false,
		}).addTo(platina);

		await platina.once("render");
		return expectPixelmatch(context, "spec/11-wirestroke/miter", 82, 10);
	});

	it("Miter + centerline", async function () {
		const [platina, context] = spawnPlatina();

		new WireStroke(geom, {
			colour: "black",
			width: 20,
			joins: WireStroke.MITER,
			centerline: true,
		}).addTo(platina);

		await platina.once("render");
		return expectPixelmatch(context, "spec/11-wirestroke/miter-centerline", 85, 10);
	});

	it("Bevel", async function () {
		const [platina, context] = spawnPlatina();

		new WireStroke(geom, {
			colour: "black",
			width: 20,
			joins: WireStroke.BEVEL,
			centerline: false,
		}).addTo(platina);

		await platina.once("render");
		return expectPixelmatch(context, "spec/11-wirestroke/bevel", 51, 10);
	});

	it("Bevel + centerline", async function () {
		const [platina, context] = spawnPlatina();

		new WireStroke(geom, {
			colour: "black",
			width: 20,
			joins: WireStroke.BEVEL,
			centerline: true,
		}).addTo(platina);

		await platina.once("render");
		return expectPixelmatch(context, "spec/11-wirestroke/bevel-centerline", 55, 10);
	});

	it("Outbevel", async function () {
		const [platina, context] = spawnPlatina();

		new WireStroke(geom, {
			colour: "black",
			width: 20,
			joins: WireStroke.OUTBEVEL,
			centerline: false,
		}).addTo(platina);

		await platina.once("render");
		return expectPixelmatch(context, "spec/11-wirestroke/outbevel", 56, 10);
	});

	it("Outbevel + centerline", async function () {
		const [platina, context] = spawnPlatina();

		new WireStroke(geom, {
			colour: "black",
			width: 20,
			joins: WireStroke.OUTBEVEL,
			centerline: true,
		}).addTo(platina);

		await platina.once("render");
		return expectPixelmatch(
			context,
			"spec/11-wirestroke/outbevel-centerline",
			55,
			10
		);
	});
});

const geomLoop = new Geometry(cartesian, [
	[0.2, 0.1],
	[0.6, 0.7],
	[0.9, -0.3],
	[0.9, -1.2],
	[0.1, -0.7],
	[0.6, -0.32],
	[-0.5, -0.22],
	[0.2, 0.1],
]);

describe("Wirestroke loop", function () {
	it("Miter", async function () {
		const [platina, context] = spawnPlatina();

		new WireStroke(geomLoop, {
			colour: "black",
			width: 20,
			joins: WireStroke.MITER,
			centerline: false,
		}).addTo(platina);

		await platina.once("render");
		return expectPixelmatch(context, "spec/11-wirestroke/loop-miter", 53, 10);
	});

	it("Miter + centerline", async function () {
		const [platina, context] = spawnPlatina();

		new WireStroke(geomLoop, {
			colour: "black",
			width: 20,
			joins: WireStroke.MITER,
			centerline: true,
		}).addTo(platina);

		await platina.once("render");
		return expectPixelmatch(
			context,
			"spec/11-wirestroke/loop-miter-centerline",
			55,
			10
		);
	});

	it("Bevel", async function () {
		const [platina, context] = spawnPlatina();

		new WireStroke(geomLoop, {
			colour: "black",
			width: 20,
			joins: WireStroke.BEVEL,
			centerline: false,
		}).addTo(platina);

		await platina.once("render");
		return expectPixelmatch(context, "spec/11-wirestroke/loop-bevel", 45, 10);
	});

	it("Bevel + centerline", async function () {
		const [platina, context] = spawnPlatina();

		new WireStroke(geomLoop, {
			colour: "black",
			width: 20,
			joins: WireStroke.BEVEL,
			centerline: true,
		}).addTo(platina);

		await platina.once("render");
		return expectPixelmatch(
			context,
			"spec/11-wirestroke/loop-bevel-centerline",
			55,
			32
		);
	});

	it("Outbevel", async function () {
		const [platina, context] = spawnPlatina();

		new WireStroke(geomLoop, {
			colour: "black",
			width: 20,
			joins: WireStroke.OUTBEVEL,
			centerline: false,
		}).addTo(platina);

		await platina.once("render");
		return expectPixelmatch(context, "spec/11-wirestroke/loop-outbevel", 33, 10);
	});

	it("Bevel + centerline", async function () {
		const [platina, context] = spawnPlatina();

		new WireStroke(geomLoop, {
			colour: "black",
			width: 20,
			joins: WireStroke.BEVEL,
			centerline: true,
		}).addTo(platina);

		await platina.once("render");
		return expectPixelmatch(
			context,
			"spec/11-wirestroke/loop-bevel-centerline",
			49,
			32
		);
	});
});

describe("Wirestroke diagonal", function () {
	it("Regular, miter", async function () {
		const [platina, context] = spawnPlatina();

		new WireStroke(
			new Geometry(cartesian, [
				[-0.5, -0.5],
				[0.5, 0.5],
			]),
			{
				colour: "black",
				width: 20,
				joins: WireStroke.MITER,
			}
		).addTo(platina);

		await platina.once("render");
		return expectPixelmatch(context, "spec/11-wirestroke/diagonal", 10, 10);
	});

	it("Regular, bevel", async function () {
		const [platina, context] = spawnPlatina();

		new WireStroke(
			new Geometry(cartesian, [
				[-0.5, -0.5],
				[0.5, 0.5],
			]),
			{
				colour: "black",
				width: 20,
				joins: WireStroke.BEVEL,
			}
		).addTo(platina);

		await platina.once("render");
		return expectPixelmatch(context, "spec/11-wirestroke/diagonal", 10, 10);
	});

	it("Degenerate loop, miter", async function () {
		const [platina, context] = spawnPlatina();

		new WireStroke(
			new Geometry(cartesian, [
				[-0.5, -0.5],
				[0.5, 0.5],
				[-0.5, -0.5],
			]),
			{
				colour: "black",
				width: 20,
				joins: WireStroke.MITER,
			}
		).addTo(platina);

		await platina.once("render");
		return expectPixelmatch(
			context,
			"spec/11-wirestroke/diagonal-loop-miter",
			32,
			32
		);
	});

	it("Degenerate loop, bevel", async function () {
		const [platina, context] = spawnPlatina();

		new WireStroke(
			new Geometry(cartesian, [
				[-0.5, -0.5],
				[0.5, 0.5],
				[-0.5, -0.5],
			]),
			{
				colour: "black",
				width: 20,
				joins: WireStroke.BEVEL,
			}
		).addTo(platina);

		await platina.once("render");
		return expectPixelmatch(
			context,
			"spec/11-wirestroke/diagonal-loop-bevel",
			32,
			32
		);
	});
});
