import Geometry from "../../src/geometry/Geometry.mjs";
import Platina from "../../src/Platina.mjs";
import Fill from "../../src/symbols/Fill.mjs";
import Stroke from "../../src/symbols/Stroke.mjs";
import cartesian from "../../src/crs/cartesian.mjs";
import { setFactory } from "../../src/geometry/DefaultGeometry.mjs";
import ditherify from "../../src/symboldecorators/ditherify.mjs";

let headless = false;
try {
	window;
} catch (ex) {
	headless = true;
}

setFactory(function cartesianize(coords) {
	return new Geometry(cartesian, coords);
});

function spawnPlatina() {
	const context = contextFactory(256, 256, { preserveDrawingBuffer: true });
	return [
		new Platina(context, {
			crs: cartesian,
			center: new Geometry(cartesian, [0, 0]),
			scale: 0.1,
			preserveDrawingBuffer: true,
		}),
		context,
	];
}

// Seedable pseudo-random function, from https://stackoverflow.com/questions/521295/seeding-the-random-number-generator-in-javascript
function sfc32(a, b, c, d) {
	return function () {
		a >>>= 0;
		b >>>= 0;
		c >>>= 0;
		d >>>= 0;
		var t = (a + b) | 0;
		a = b ^ (b >>> 9);
		b = (c + (c << 3)) | 0;
		c = (c << 21) | (c >>> 11);
		d = (d + 1) | 0;
		t = (t + d) | 0;
		c = (c + t) | 0;
		return (t >>> 0) / 4294967296;
	};
}

// **Seeded** pseudorandom texture
const pseudorandom = sfc32(1111, 2222, 3333, 4444);

// Generate a 512x512 seeded pseudorandom texture. It will replace the automatically
// generated one. We need the same texture every time since the built-in
// Math.random() is not seedable.
const textureData = Float32Array.from(new Array(512 * 512), pseudorandom);

// Shorthand for replacing the noise texture of an acetate with the precalculated
// stable pseudorandom noise
function replaceNoise(ditherAcetate) {
	ditherAcetate.once("programlinked", () => {
		const glii = ditherAcetate.glii;
		ditherAcetate._program.setTexture(
			"uDitherNoise",
			new glii.Texture({
				format: glii.gl.RED,
				internalFormat: glii.gl.R32F,
				type: glii.FLOAT,
				wrapS: glii.REPEAT,
				wrapT: glii.REPEAT,
			}).texArray(512, 512, textureData)
		);
	});
}

describe("Dither", function () {
	it("Polygon fill", async function () {
		if (headless) {
			pending("Dither disabled on headless environments");
		}

		const [platina, context] = spawnPlatina();

		const DitherFill = ditherify(Fill);

		replaceNoise(new DitherFill.Acetate(platina));

		let concave = new DitherFill(
			[
				[-10, -10],
				[-5, 8],
				[0, -5],
				[5, 10],
				[10, -10],
				[-10, -10],
			],
			{
				colour: "black",
				dither: 0.2,
			}
		).addTo(platina);

		await platina.once("render");
		await expectPixelmatch(context, "spec/17-ditherify/dither-fill-black", 10, 10);

		concave.colour = "blue";

		await platina.once("render");
		return await expectPixelmatch(
			context,
			"spec/17-ditherify/dither-fill-blue",
			10,
			10
		);
	});

	it("Stroke", async function () {
		if (headless) {
			pending("Dither disabled on headless environments");
		}

		const [platina, context] = spawnPlatina();

		const DitherStroke = ditherify(Stroke);

		replaceNoise(new DitherStroke.Acetate(platina));

		let zigzag = new DitherStroke(
			[
				[-10, -10],
				[-5, 8],
				[0, -5],
				[5, 10],
				[10, -10],
			],
			{
				colour: "black",
				width: 20,
				dither: 0.2,
			}
		).addTo(platina);

		await platina.once("render");
		await expectPixelmatch(context, "spec/17-ditherify/dither-stroke-black", 10, 10);

		zigzag.colour = "blue";

		await platina.once("render");
		return await expectPixelmatch(
			context,
			"spec/17-ditherify/dither-stroke-blue",
			10,
			10
		);
	});
});
