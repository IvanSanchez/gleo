import Geometry from "../../src/geometry/Geometry.mjs";
import Platina from "../../src/Platina.mjs";
import cartesian from "../../src/crs/cartesian.mjs";
import Sprite from "../../src/symbols/Sprite.mjs";
import hueshiftify from "../../src/symboldecorators/hueshiftify.mjs";
import { setFactory } from "../../src/geometry/DefaultGeometry.mjs";
import blueMarker from "./blueMarker.mjs";

let headless = false;
try {
	window;
} catch (ex) {
	headless = true;
}

// These tests seem to time out in CI servers more often than any other test (or the same
// as the TintedSprite tests).
jasmine.DEFAULT_TIMEOUT_INTERVAL = 20000;

setFactory(function cartesianize(coords) {
	return new Geometry(cartesian, coords);
});

function spawnPlatina() {
	const context = contextFactory(256, 256, { preserveDrawingBuffer: true });
	return [
		new Platina(context, {
			crs: cartesian,
			center: [0, 0],
			scale: 2,
			preserveDrawingBuffer: true,
		}),
		context,
	];
}

describe("hueshifted Sprites", function () {
	it("Multiple", async function () {
		if (headless) {
			pending(
				"Headless environment lacks support for either HTMLImageElement or ImageData"
			);
		}
		const [platina, context] = spawnPlatina();

		const ShiftSprite = hueshiftify(Sprite);

		const shiftSpriteAcetate = new ShiftSprite.Acetate(platina);

		const spriteOpts = {
			image: blueMarker,
			spriteAnchor: [13, 41],
		};

		const sprites = [
			new ShiftSprite([-120, 0], { ...spriteOpts, hueShift: 0 }),
			new ShiftSprite([-80, 0], { ...spriteOpts, hueShift: 60 }),
			new ShiftSprite([-40, 0], { ...spriteOpts, hueShift: 120 }),
			new ShiftSprite([0, 0], { ...spriteOpts, hueShift: 180 }),
			new ShiftSprite([40, 0], { ...spriteOpts, hueShift: 240 }),
			new ShiftSprite([80, 0], { ...spriteOpts, hueShift: 300 }),
			new ShiftSprite([120, 0], { ...spriteOpts, hueShift: 360 }),
		];

		platina.multiAdd(sprites);

		// Images do take time to load
		await new Promise((res) => shiftSpriteAcetate.on("symbolsadded", res));

		await platina.once("render");

		await expectPixelmatch(
			context,
			"spec/22-hueshift/hueshift-sprite-multiple",
			100,
			0
		);
	});
});
