/// NOTE: Change this value to run the tests in windowed, debuggeable mode.
let headless = true;

// Are we running inside a continuous integration environment?
let ci = !!process.env.CI;

// Not really an array of browsers, but rather an set of
// sets of puppetteer.launch options.
let browsers = {
	firefox: {
		product: "firefox",
		// executablePath: "/usr/bin/firefox",
		args: ["-wait-for-browser"],
		// Can't use WebGL in headless mode (https://bugzil.la/1375585):
		headless: false,
		devtools: !headless,
		protocol: "webDriverBiDi",
	},

	// "firefox-esr": {
	// 	product: "firefox",
	// 	executablePath: "/usr/bin/firefox-esr",
	// 	//args: ["-wait-for-browser"],
	// 	// Can't use WebGL in headless mode (https://bugzil.la/1375585):
	// 	headless: false,
	// 	devtools: !headless,
	// },

	// "firefox-local": {
	// 	product: "firefox",
	// 	executablePath: "/home/ivan/bin/firefox-nightly",
	// 	args: ["-wait-for-browser"],
	// 	// Can't use WebGL in headless mode (https://bugzil.la/1375585):
	// 	headless: false,
	// 	devtools: !headless,
	// },

	"chromium-swiftshader": {
		product: "chrome",
		// executablePath: "chromium",
		args: [
			"--use-gl=swiftshader",
			ci ? "--no-sandbox" : "",
			ci ? "--disable-setuid-sandbox" : "",
		],
		headless: false,
		devtools: !headless,
	},

	// "chromium-egl": {
	// 	product: "chrome",
	// // 	executablePath: "chromium",
	// 	args: [
	// 		"--use-gl=egl",
	// 		ci ? "--no-sandbox" : "",
	// 		ci ? "--disable-setuid-sandbox" : "",
	// 	],
	// 	headless: headless,
	// 	devtools: !headless,
	// },
};

import puppeteer from "puppeteer";

import handler from "serve-handler";
import http from "http";

const port = 7000;

const server = http.createServer((request, response) => {
	// You pass two more arguments for config and middleware
	// More details here: https://github.com/vercel/serve-handler#options
	return handler(request, response);
});

server.listen(port, () => {
	console.log("temp webserver running at http://localhost:7000");
});

let passing = true;

for (const [browser, config] of Object.entries(browsers)) {
	console.log(`Spawning browser: ${browser}`);
	await run(config);
	console.log("\n");
}

async function run(launchOptions) {
	const browser = await puppeteer.launch(launchOptions).catch((ex) => {
		// Gracefully skip tests if firefox/chromium is not installed
		// The error message thrown by puppeteer is informative enough
		console.warn(ex);
		return false;
	});

	if (!browser) {
		return;
	}

	const pixelmatchRegexp = /pixelmatch-error-([\d]+)/;

	const page = await browser.newPage();

	const timeout = setTimeout(() => {
		console.error("Timing out");
		process.exit(-2);
	}, 30000);

	let suitesFinished;
	const waitForSuite = new Promise((res, rej) => {
		suitesFinished = res;
	});

	page.on("console", (message) => {
		const type = message.type().substr(0, 3).toUpperCase();
		const fns = {
			LOG: console.log,
			ERR: console.error,
			WAR: console.warn,
			INF: console.info,
		};
		fns[type](message.text());

		if (message.text() === "Finished suite: failed") {
			clearTimeout(timeout);
			suitesFinished();
			if (headless) {
				return (passing = false);
			}
		}
		if (message.text() === "Finished suite: passed") {
			clearTimeout(timeout);
			suitesFinished();
			return passing;
		}

		let match;
		if ((match = pixelmatchRegexp.exec(message.text()))) {
			// console.log("pixelmatch error caught:", match);
			page.$("#" + match[0]).then((el) => {
				console.log("Screenshotting #" + match[0]);

				el.screenshot({
					path: `spec/mismatch-${launchOptions.product}-${match[1]}.png`,
				});
			});
		}
	});

	//   await page.goto('https://127.0.0.1:5000', {waitUntil: 'networkidle2'});
	await page.goto(`http://127.0.0.1:${port}/spec/SpecRunner.html`, {
		// 		waitUntil: "networkidle",
		// 		waitUntil: "networkidle2",
	});
	//   await page.goto(runner, {waitUntil: 'networkidle2'});

	await waitForSuite;

	// Manually wait a bit in case there's screenshotting to be done.
	await new Promise((res) => setTimeout(res, 1000));

	await browser.close();
}

if (passing) {
	process.exit(0);
} else {
	process.exit(-1);
}
