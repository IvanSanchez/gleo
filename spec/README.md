The scaffolding for these tests is just ripped from Glii. Both test runners should be
kept in sync (in regards to dependencies, etc)

If puppeteer does not detect installed web browsers, run (as needed):

npx puppeteer browsers install firefox
npx puppeteer browsers install firefox@esr
npx puppeteer browsers install chromium@latest
