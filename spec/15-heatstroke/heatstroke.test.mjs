import Geometry from "../../src/geometry/Geometry.mjs";
import Platina from "../../src/Platina.mjs";
import HeatStroke from "../../src/symbols/HeatStroke.mjs";
import cartesian from "../../src/crs/cartesian.mjs";
import GreyscaleField from "../../src/fields/GreyscaleField.mjs";
import HeatMap from "../../src/fields/HeatMap.mjs";

let headless = false;
try {
	window;
} catch (ex) {
	headless = true;
}

function spawnPlatina() {
	const context = contextFactory(384, 384, { preserveDrawingBuffer: true });
	return [
		new Platina(context, {
			crs: cartesian,
			center: [0, 0],
			scale: 0.75,
			preserveDrawingBuffer: true,
			backgroundColour: "white",
		}),
		context,
	];
}

const geoms = {
	Squiggle: new Geometry(cartesian, [
		[0.2, 0.1],
		[0.6, 0.7],
		[0.9, -0.3],
		[-0.5, -0.3],
		[0.1, -0.7],
		[0.8, -0.7],
		[0.05, -1.2],
		[0.2, 0.1],
	]),
	Diagonal: new Geometry(cartesian, [
		[0.9, -0.9],
		[-0.8, 0.8],
	]),
};

describe("Heatstroke", function () {
	for (const [name, geom] of Object.entries(geoms)) {
		it(`${name}, on greyscale field`, async function () {
			if (headless) {
				pending("Headless environment lacks render-to-float32-textures support");
			}
			const [platina, context] = spawnPlatina();

			const field = new GreyscaleField(platina, {});

			const heatstroke = new HeatStroke(geom, { width: 40, intensity: 1 });
			platina.multiAdd([heatstroke]);
			platina.scale = 0.006;

			await platina.once("render");

			await expectPixelmatch(
				context,
				`spec/15-heatstroke/${name}-greyscalefield`,
				0,
				0
			);

			// Symbols **must** be manually removed from the platina.
			// If not, they'll hold a stale reference to their acetates when
			// they're added to a different platina.
			platina.multiRemove([heatstroke]);
		});

		it(`${name}, on heatmap`, async function () {
			if (headless) {
				pending("Headless environment lacks render-to-float32-textures support");
			}
			const [platina, context] = spawnPlatina();

			const field = new HeatMap(platina, {});

			const heatstroke = new HeatStroke(geom, {
				width: 40,
				intensity: 100,
				// joins: HeatStroke.MITER
			});
			platina.multiAdd([heatstroke]);
			platina.scale = 0.006;

			await platina.once("render");

			await expectPixelmatch(context, `spec/15-heatstroke/${name}-heatmap`, 0, 0);

			// Symbols **must** be manually removed from the platina.
			// If not, they'll hold a stale reference to their acetates when
			// they're added to a different platina.
			platina.multiRemove([heatstroke]);
		});
	}
});
