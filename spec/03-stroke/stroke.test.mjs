import Geometry from "../../src/geometry/Geometry.mjs";
import Platina from "../../src/Platina.mjs";
import Stroke from "../../src/symbols/Stroke.mjs";
import Dot from "../../src/symbols/Dot.mjs";
import cartesian from "../../src/crs/cartesian.mjs";
import epsg4326 from "../../src/crs/epsg4326.mjs";
import LatLng from "../../src/geometry/LatLng.mjs";

function spawnPlatina() {
	const context = contextFactory(256, 256, { preserveDrawingBuffer: true });
	return [
		new Platina(context, {
			crs: cartesian,
			center: new Geometry(cartesian, [0, 0]),
			scale: 0.1,
			preserveDrawingBuffer: true,
		}),
		context,
	];
}

const joinTypes = {
	miter: Stroke.MITER,
	bevel: Stroke.BEVEL,
	outbevel: Stroke.OUTBEVEL,
};

const endcapTypes = {
	butt: Stroke.BUTT,
	square: Stroke.SQUARE,
};

for (const [joinName, joinValue] of Object.entries(joinTypes)) {
	for (const [capName, capValue] of Object.entries(endcapTypes)) {
		describe(`Stroke, ${joinName}-${capName}`, function () {
			const opts = {
				joins: joinValue,
				caps: capValue,
			};

			it(`zig-zag`, async function () {
				const [platina, context] = spawnPlatina();

				new Stroke(
					new Geometry(cartesian, [
						[-10, -10],
						[-5, 8],
						[0, -5],
						[5, 10],
						[10, -10],
					]),
					{
						colour: `black`,
						width: 16,
						...opts,
					}
				).addTo(platina);

				await platina.once(`render`);
				return expectPixelmatch(
					context,
					`spec/03-stroke/${joinName}-${capName}-zigzag`,
					11,
					10
				);
			});
			it(`zig-zag, update colour/dash`, async function () {
				const [platina, context] = spawnPlatina();

				let zigzag = new Stroke(
					new Geometry(cartesian, [
						[-10, -10],
						[-5, 8],
						[0, -5],
						[5, 10],
						[10, -10],
					]),
					{
						colour: `black`,
						width: 16,
						...opts,
					}
				).addTo(platina);

				await platina.once(`render`);
				await expectPixelmatch(
					context,
					`spec/03-stroke/${joinName}-${capName}-zigzag`,
					15,
					10
				);

				zigzag.colour = `red`;

				await platina.once(`render`);
				await expectPixelmatch(
					context,
					`spec/03-stroke/${joinName}-${capName}-zigzag-red`,
					11,
					10
				);

				zigzag.dashArray = [30, 30];

				await platina.once(`render`);
				await expectPixelmatch(
					context,
					`spec/03-stroke/${joinName}-${capName}-zigzag-dash`,
					20,
					10
				);

				zigzag.colour = `blue`;

				await platina.once(`render`);
				await expectPixelmatch(
					context,
					`spec/03-stroke/${joinName}-${capName}-zigzag-blue`,
					16,
					10
				);
			});

			const strokeOpts = {
				colour: `black`,
				width: 32,
				...opts,
			};

			it(`square loop, counterclockwise, top-right start`, async function () {
				// console.log(`square loop, counterclockwise, top-right start`);
				const [platina, context] = spawnPlatina();

				// Clockwise
				let square1 = new Stroke(
					new Geometry(cartesian, [
						[10, 10],
						[-10, 10],
						[-10, -10],
						[10, -10],
						[10, 10],
					]),
					strokeOpts
				).addTo(platina);

				await platina.once(`render`);
				await expectPixelmatch(
					context,
					`spec/03-stroke/${joinName}-${capName}-square`,
					0,
					0
				);
			});

			it(`square loop, counterclockwise, bottom-right start`, async function () {
				// console.log(`square loop, counterclockwise, bottom-right start`);
				const [platina, context] = spawnPlatina();

				// Clockwise
				let square2 = new Stroke(
					new Geometry(cartesian, [
						[10, -10],
						[10, 10],
						[-10, 10],
						[-10, -10],
						[10, -10],
					]),
					strokeOpts
				).addTo(platina);

				await platina.once(`render`);
				await expectPixelmatch(
					context,
					`spec/03-stroke/${joinName}-${capName}-square`,
					0,
					0
				);
			});

			it(`square loop, counterclockwise, top-left start`, async function () {
				// console.log(`square loop, counterclockwise, top-left start`);
				const [platina, context] = spawnPlatina();

				// Clockwise
				let square1 = new Stroke(
					new Geometry(cartesian, [
						[-10, 10],
						[-10, -10],
						[10, -10],
						[10, 10],
						[-10, 10],
					]),
					strokeOpts
				).addTo(platina);

				await platina.once(`render`);
				await expectPixelmatch(
					context,
					`spec/03-stroke/${joinName}-${capName}-square`,
					0,
					0
				);
			});

			it(`square loop, counterclockwise, bottom-right start`, async function () {
				// console.log(`square loop, counterclockwise, bottom-right start`);
				const [platina, context] = spawnPlatina();

				// Clockwise
				let square2 = new Stroke(
					new Geometry(cartesian, [
						[-10, -10],
						[10, -10],
						[10, 10],
						[-10, 10],
						[-10, -10],
					]),
					strokeOpts
				).addTo(platina);

				await platina.once(`render`);
				await expectPixelmatch(
					context,
					`spec/03-stroke/${joinName}-${capName}-square`,
					0,
					0
				);
			});

			it(`square loop, clockwise, bottom-left start`, async function () {
				// console.log(`square loop, clockwise, bottom-left start`);
				const [platina, context] = spawnPlatina();

				// Counter-clockwise
				let square2 = new Stroke(
					new Geometry(cartesian, [
						[-10, -10],
						[-10, 10],
						[10, 10],
						[10, -10],
						[-10, -10],
					]),
					strokeOpts
				).addTo(platina);

				await platina.once(`render`);
				await expectPixelmatch(
					context,
					`spec/03-stroke/${joinName}-${capName}-square`,
					0,
					0
				);
			});

			it(`hollow multipolygons (multiple hulls and multiple rings)`, async function () {
				const [platina, context] = spawnPlatina();

				// Counter-clockwise
				new Stroke(
					new Geometry(cartesian, [
						[
							[
								[-10, -4],
								[-10, 4],
								[-2, 4],
								[-2, -4],
								[-10, -4],
							],
							[
								[-8, -2],
								[-8, 2],
								[-4, 2],
								[-4, -2],
								[-8, -2],
							],
						],
						[
							[
								[10, -4],
								[10, 4],
								[2, 4],
								[2, -4],
								[10, -4],
							],
							[
								[8, -2],
								[8, 2],
								[4, 2],
								[4, -2],
								[8, -2],
							],
						],
					]),
					{ width: 10, colour: `black`, ...opts }
				).addTo(platina);

				await platina.once(`render`);
				await expectPixelmatch(
					context,
					`spec/03-stroke/${joinName}-${capName}-hollows`,
					0,
					0
				);
			});

			it(`degenerate geometries (empty hull)`, async function () {
				const [platina, context] = spawnPlatina();

				// Counter-clockwise
				let degen = new Stroke(
					new Geometry(cartesian, [
						[
							[2, 3],
							[2, 3],
							[2, 3],
							[2, 3],
						],
						[
							[-10, -10],
							[-10, 10],
							[10, 10],
							[10, -10],
							[-10, -10],
						],
					]),
					strokeOpts
				).addTo(platina);

				await platina.once(`render`);
				await expectPixelmatch(
					context,
					`spec/03-stroke/${joinName}-${capName}-square`,
					0,
					0
				);
			});

			it(`small acute angle`, async function () {
				const [platina, context] = spawnPlatina();
				new Stroke(
					new Geometry(cartesian, [
						[0, 0.05],
						[-0.4, 0],
						[0, -0.05],
					]),
					{
						width: 40,
						colour: `black`,
						...opts,
					}
				).addTo(platina);

				await platina.once(`render`);
				await expectPixelmatch(
					context,
					`spec/03-stroke/${joinName}-${capName}-small-acute`,
					40,
					0
				);
			});
		});
	}
}
