// Define, as a global, `contextFactory`.

const ctxsMap = {};

window.contextFactory2d = function contextFactory(w, h, opts) {
	if (ctxsMap[w + "+" + h]) {
		return ctxsMap[w + "+" + h];
	}
	const canvas = document.createElement("canvas");
	canvas.height = h;
	canvas.width = w;

	const ctx = canvas.getContext("2d");
	ctx.clearRect(0, 0, w, h);
	// 	document.body.appendChild(canvas);
	return (ctxsMap[w + "+" + h] = canvas);
};
