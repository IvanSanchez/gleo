// Some of the code for GeoJSON uses `document.URL` which should point
// at the current working directory.

// See also: https://github.com/nodejs/undici/issues/2751

import process from "process";
import fs from "fs";

global.document = {
	// URL: new URL(__dirname + "/../", "file:")
	URL: new URL(process.cwd() + "/spec/", "file:"),
};

global.openAsBlob = function (path) {
	return fs.openAsBlob(path);
};
