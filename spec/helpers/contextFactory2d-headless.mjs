// Define, as a global, `contextFactory`.

import { GlobalFonts, createCanvas } from "@napi-rs/canvas";

// console.log(GlobalFonts, GlobalFonts.families);

global.contextFactory2d = createCanvas;

// Create a global WebGL2RenderingContext, just to lie to some checks in Platina.resize()
global.WebGL2RenderingContext ??= class DoesNotExist {};

global.devicePixelRatio = 1;
