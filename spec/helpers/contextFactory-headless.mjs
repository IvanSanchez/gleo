// Define, as a global, `contextFactory`.

import { default as gl } from "gl";
global.contextFactory = function fakeCanvas(...args) {
	const instance = gl(...args);

	instance.addEventListener = function addEventListener() {};
	instance.removeEventListener = function removeEventListener() {};

	return instance;
};
