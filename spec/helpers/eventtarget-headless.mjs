// Gleo code expects DOM's `EventTarget` to be globally available
// (as a property of `window`)

// NodeJS adds a `EventTarget` implementation at node v14.5

import { EventEmitter } from "events";
import { setImmediate, clearImmediate } from "timers";

global.EventTarget = class EventTarget {
	#emitter;
	constructor() {
		this.#emitter = new EventEmitter();
	}
	dispatchEvent(ev) {
		this.#emitter.emit(ev.type, ev.stuff);
	}
	addEventListener(...args) {
		this.#emitter.on(...args);
	}
	removeEventListener(...args) {
		this.#emitter.off(...args);
	}
};

global.Event = class Event {
	constructor(type, stuff) {
		this.type = type;
		this.stuff = stuff;
	}
};
global.CustomEvent = Event;
global.MouseEvent = Event;
global.PointerEvent = Event;

global.ResizeObserver = class ResizeObserver {
	observe() {}
};

global.requestAnimationFrame = setImmediate;
global.cancelAnimationFrame = clearImmediate;

global.devicePixelRatio = 1;
