import Geometry from "../../src/geometry/Geometry.mjs";
import Platina from "../../src/Platina.mjs";
// import Stroke from "../../src/symbols/Stroke.mjs";
// import Dot from "../../src/symbols/Dot.mjs";
import cartesian from "../../src/crs/cartesian.mjs";
import HeatPoint from "../../src/symbols/HeatPoint.mjs";
import GreyscaleField from "../../src/fields/GreyscaleField.mjs";
import { setFactory } from "../../src/geometry/DefaultGeometry.mjs";

let headless = false;
try {
	window;
} catch (ex) {
	headless = true;
}

setFactory(function cartesianize(coords) {
	return new Geometry(cartesian, coords);
});

function spawnPlatina() {
	const context = contextFactory(384, 384, { preserveDrawingBuffer: true });
	return [
		new Platina(context, {
			crs: cartesian,
			center: [0, 0],
			scale: 0.75,
			preserveDrawingBuffer: true,
			backgroundColour: "white",
		}),
		context,
	];
}

const points1 = [
	new HeatPoint([10, 10], { radius: 60, intensity: 20 }),
	new HeatPoint([20, 10], { radius: 60, intensity: 10 }),
	new HeatPoint([-20, -10], { radius: 60, intensity: 10 }),
	new HeatPoint([-20, -45], { radius: 60, intensity: 30 }),
	new HeatPoint([-40, -20], { radius: 60, intensity: 10 }),
];

const points2 = [
	new HeatPoint([169.3, 138.9], { radius: 60, intensity: 20 }),
	new HeatPoint([129.6, 169.0], { radius: 60, intensity: 10 }),
	new HeatPoint([142.7, -125.4], { radius: 60, intensity: 10 }),
	new HeatPoint([-164.2, 193.2], { radius: 60, intensity: 30 }),
	new HeatPoint([-165.3, 107.6], { radius: 60, intensity: 10 }),
	new HeatPoint([-91.1, -146.8], { radius: 60, intensity: 20 }),
	new HeatPoint([32.2, -58.7], { radius: 60, intensity: 10 }),
	new HeatPoint([174.6, -184.6], { radius: 60, intensity: 10 }),
	new HeatPoint([71.2, 130.9], { radius: 60, intensity: 30 }),
	new HeatPoint([37.6, -72.8], { radius: 60, intensity: 10 }),
	new HeatPoint([-67.3, 109.6], { radius: 60, intensity: 20 }),
	new HeatPoint([-49.5, -213.2], { radius: 60, intensity: 10 }),
	new HeatPoint([-189.5, -15.2], { radius: 60, intensity: 10 }),
	new HeatPoint([-146.6, 183.2], { radius: 60, intensity: 30 }),
	new HeatPoint([54.7, -184.0], { radius: 60, intensity: 10 }),
	new HeatPoint([79.4, 157.3], { radius: 60, intensity: 20 }),
	new HeatPoint([132.9, -156.5], { radius: 60, intensity: 10 }),
];

console.log("headless:", headless);

describe("Greyscale field", function () {
	it("One point", async function () {
		if (headless) {
			pending("Headless environment lacks render-to-float32-textures support");
		}
		const [platina, context] = spawnPlatina();

		const field = new GreyscaleField(platina, {});

		const points = [new HeatPoint([0, 0], { radius: 60, intensity: 1 })];
		platina.multiAdd(points);

		await platina.once("render");

		await expectPixelmatch(context, "spec/14-greyscale-field/onepoint", 0, 0);

		// The point at the center would fall at pixel coordinates 192, 192
		// (since the size of the canvas is a multiple of 2). Ideally
		// that pixel would have a value of 1.
		expect(field.getFieldValueAt(192, 192)).toBeGreaterThan(0.985);
		expect(field.getFieldValueAt(192 - 61, 192)).toEqual(0);
		expect(field.getFieldValueAt(192 - 31, 192)).toBeGreaterThan(0.49);
		expect(field.getFieldValueAt(192 - 31, 192)).toBeLessThan(0.5);
		expect(field.getFieldValueAt(0, 0)).toEqual(0);

		// Symbols **must** be manually removed from the platina.
		// If not, they'll hold a stale reference to their acetates when
		// they're added to a different platina.
		platina.multiRemove(points);
	});

	it("Point set 1, greyscale", async function () {
		if (headless) {
			pending("Headless environment lacks render-to-float32-textures support");
		}
		const [platina, context] = spawnPlatina();

		const field = new GreyscaleField(platina, {
			minValue: 0,
			maxValue: 30,
		});

		platina.multiAdd(points1);

		await platina.once("render");

		await expectPixelmatch(context, "spec/14-greyscale-field/greyscale", 0, 0);

		// Symbols **must** be manually removed from the platina.
		// If not, they'll hold a stale reference to their acetates when
		// they're added to a different platina.
		platina.multiRemove(points1);
	});

	it("Point set 2, greyscale", async function () {
		if (headless) {
			pending("Headless environment lacks render-to-float32-textures support");
		}
		const [platina, context] = spawnPlatina();

		const field = new GreyscaleField(platina, {
			minValue: 0,
			maxValue: 30,
		});

		field.multiAdd(points1);
		platina.multiAdd(points2);

		await platina.once("render");

		await expectPixelmatch(context, "spec/14-greyscale-field/greyscaleplus", 0, 0);
		platina.multiRemove(points1);
		platina.multiRemove(points2);
	});

	it("Point set 1, blue-red", async function () {
		if (headless) {
			pending("Headless environment lacks render-to-float32-textures support");
		}
		const [platina, context] = spawnPlatina();

		const field = new GreyscaleField(platina, {
			minValue: 0,
			maxValue: 30,
			minColour: "blue",
			maxColour: "red",
		});

		field.multiAdd(points1);

		await platina.once("render");

		await expectPixelmatch(context, "spec/14-greyscale-field/blueredscale", 0, 0);

		platina.multiRemove(points1);
	});
});
