import Geometry from "../../src/geometry/Geometry.mjs";
import Platina from "../../src/Platina.mjs";
// import Pie from "../../src/symbols/Pie.mjs";
import CircleFill from "../../src/symbols/CircleFill.mjs";
import Fill from "../../src/symbols/Fill.mjs";
import GeoJSON from "../../src/loaders/GeoJSON.mjs";
import SymbolGroup from "../../src/loaders/SymbolGroup.mjs";
import epsg3857 from "../../src/crs/epsg3857.mjs";
import epsg4326 from "../../src/crs/epsg4326.mjs";

function spawnPlatina() {
	const context = contextFactory(256, 256, { preserveDrawingBuffer: true });
	return [
		new Platina(context, {
			crs: epsg3857,
			center: new Geometry(epsg4326, [-3, 40]),
			scale: 8000,
			preserveDrawingBuffer: true,
			backgroundColour: [255, 255, 255, 255],
		}),
		context,
	];
}

let headless = false;
try {
	window;
} catch (ex) {
	headless = true;
}

async function geoJsonBlob() {
	return headless
		? await openAsBlob("./spec/20-geojson/spain.geojson")
		: await (await fetch("./20-geojson/spain.geojson")).blob();
}

describe(`GeoJSON`, function () {
	it("loads from fetch, default", async function () {
		if (headless) {
			return pending(
				"GeoJSON fetch disabled for headless, see https://github.com/nodejs/undici/issues/2751"
			);
		}

		const [platina, context] = spawnPlatina();

		let loader = new GeoJSON("./20-geojson/spain.geojson").addTo(platina);

		await loader.once("symbolsadded");
		await platina.once(`render`);
		return await expectPixelmatch(context, `spec/20-geojson/spain`, 15, 10);
	});

	it("loads from fetch, custom symbolizers", async function () {
		if (headless) {
			return pending(
				"GeoJSON fetch disabled for headless, see https://github.com/nodejs/undici/issues/2751"
			);
		}

		const [platina, context] = spawnPlatina();

		let loader = new GeoJSON("./20-geojson/spain.geojson", {
			polygonSymbolizer: (feat, geom) => {
				return new Fill(geom, { colour: "green" });
			},
			pointSymbolizer: (feat, geom) => {
				return new CircleFill(geom, { radius: 15, colour: "red" });
			},
		}).addTo(platina);

		await loader.once("symbolsadded");
		await platina.once(`render`);
		return await expectPixelmatch(context, `spec/20-geojson/spain-green-red`, 15, 10);
	});

	it("loads from blob, default ", async function () {
		const [platina, context] = spawnPlatina();

		let loader = new GeoJSON(await geoJsonBlob()).addTo(platina);

		await loader.once("symbolsadded");
		await platina.once(`render`);
		return await expectPixelmatch(context, `spec/20-geojson/spain`, 15, 10);
	});

	it("loads from blob, custom symbolizers", async function () {
		const [platina, context] = spawnPlatina();

		let loader = new GeoJSON(await geoJsonBlob(), {
			polygonSymbolizer: (feat, geom) => {
				return new Fill(geom, { colour: "green" });
			},
			pointSymbolizer: (feat, geom) => {
				return new CircleFill(geom, { radius: 15, colour: "red" });
			},
		}).addTo(platina);

		await loader.once("symbolsadded");
		await platina.once(`render`);
		return await expectPixelmatch(context, `spec/20-geojson/spain-green-red`, 15, 10);
	});

	it("nested inside SymbolGroup ", async function () {
		const [platina, context] = spawnPlatina();

		let group = new SymbolGroup();
		let loader = new GeoJSON(await geoJsonBlob()).addTo(group);

		await loader.once("symbolsadded");

		group.addTo(platina);

		await platina.once(`render`);
		return await expectPixelmatch(context, `spec/20-geojson/spain`, 15, 10);
	});
});
