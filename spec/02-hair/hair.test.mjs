import Geometry from "../../src/geometry/Geometry.mjs";
import Platina from "../../src/Platina.mjs";
import Hair from "../../src/symbols/Hair.mjs";
import cartesian from "../../src/crs/cartesian.mjs";
import epsg4326 from "../../src/crs/epsg4326.mjs";
import LatLng from "../../src/geometry/LatLng.mjs";

/**
 * NOTE: When unit-testing hairs, horizontal and vertical lines should be avoided.
 * Different platforms do rounding of coordinates differently, so off-by-one
 * differences are common. In horizontal/vertical lines, that means a lot of pixels.
 */

describe("One hair", function () {
	it("cartesian, platina options", async function () {
		const context = contextFactory(256, 256, { preserveDrawingBuffer: true });
		const platina = new Platina(context, {
			crs: cartesian,
			center: new Geometry(cartesian, [0, 0]),
			scale: 0.1,
			preserveDrawingBuffer: true,
		});

		let hair1 = new Hair(
			new Geometry(cartesian, [
				[-10, 10],
				[10, -10],
			]),
			{
				colour: "purple",
			}
		);

		platina.multiAdd([hair1]);

		await platina.once("render");
		return expectPixelmatch(context, "spec/02-hair/hair-1-purple", 0, 0);
	});

	it("cartesian, platina setters", async function () {
		const context = contextFactory(256, 256, { preserveDrawingBuffer: true });
		const platina = new Platina(context);

		let hair1 = new Hair(
			new Geometry(cartesian, [
				[10, -10],
				[-10, 10],
			]),
			{
				colour: "green",
			}
		).addTo(platina);

		platina.crs = cartesian;
		platina.setView({
			center: new Geometry(cartesian, [0, 0]),
			scale: 0.1,
		});

		await platina.once("render");
		return expectPixelmatch(context, "spec/02-hair/hair-1-green", 0, 0);
	});

	it("latlng, platina options", async function () {
		const context = contextFactory(256, 256, { preserveDrawingBuffer: true });
		const platina = new Platina(context, {
			crs: epsg4326,
			center: new LatLng([40, -120]),
			scale: 0.01,
		});

		let hair1 = new Hair(
			new LatLng([
				[39.5, -120.5],
				[41.2, -119.1],
			]),
			{
				colour: "blue",
			}
		).addTo(platina);

		await platina.once("render");
		return expectPixelmatch(context, "spec/02-hair/hair-1-blue", 0, 0);
	});
});

describe("Polygons from hairs", function () {
	it("triangle, octagon", async function () {
		const context = contextFactory(256, 256, { preserveDrawingBuffer: true });
		const platina = new Platina(context, {
			crs: cartesian,
			center: new Geometry(cartesian, [0, 0]),
			scale: 0.1,
		});

		let hair1 = new Hair(
			new Geometry(cartesian, [
				[0, 10],
				[10, 0],
				[-10, -10],
				[0, 10],
			]),
			{
				colour: "red",
			}
		);

		platina.multiAdd([hair1]);
		//platina.queueRedraw();

		await platina.once("render");
		await expectPixelmatch(context, "spec/02-hair/triangle", 0, 0);

		const octagonPoints = [];
		for (let i = 0; i < 9; i++) {
			const angle = (i * Math.PI) / 4;
			octagonPoints.push([5 * Math.sin(angle), 5 * Math.cos(angle) - 3]);
		}

		let hair2 = new Hair(new Geometry(cartesian, octagonPoints), {
			colour: "black",
		});

		platina.multiAdd([hair2]);
		await platina.once("render");
		await expectPixelmatch(context, "spec/02-hair/octagon", 16, 4);

		platina.setView({
			center: new Geometry(cartesian, [3, 3]),
			yawDegrees: 60,
		});

		await platina.once("render");
		return expectPixelmatch(context, "spec/02-hair/octagon-tilt", 20, 4);
	});
});
