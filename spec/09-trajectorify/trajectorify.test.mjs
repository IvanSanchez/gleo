import Geometry from "../../src/geometry/Geometry.mjs";
import Platina from "../../src/Platina.mjs";
import cartesian from "../../src/crs/cartesian.mjs";
import CircleFill from "../../src/symbols/CircleFill.mjs";
import CircleStroke from "../../src/symbols/CircleStroke.mjs";
import Hair from "../../src/symbols/Hair.mjs";
import trajectorify from "../../src/symboldecorators/trajectorify.mjs";
import { setFactory } from "../../src/geometry/DefaultGeometry.mjs";

setFactory(function cartesianize(coords) {
	return new Geometry(cartesian, coords);
});

function spawnPlatina() {
	const context = contextFactory(384, 384, { preserveDrawingBuffer: true });
	return [
		new Platina(context, {
			crs: cartesian,
			center: [0, 0],
			scale: 1,
			preserveDrawingBuffer: true,
		}),
		context,
	];
}

describe("Trajectorify", function () {
	it("Circles, 4 places along", async function () {
		const [platina, context] = spawnPlatina();

		const TrajectoryCircleFill = trajectorify(CircleFill);
		const TrajectoryCircleStroke = trajectorify(CircleStroke);

		const geom1 = [
			[-150, -150],
			[-120, -120],
			[-90, -100],
			[-60, -110],
			[-30, -80],
			[0, -70],
			[60, -90],
			[90, -50],
			[120, -40],
			[150, 0],
		];

		const mValues1 = [100, 500, 900, 1400, 1800, 1900, 2200, 2400, 2700, 3100];

		platina.multiAdd([
			new TrajectoryCircleFill(geom1, {
				mcoords: mValues1,
				colour: "green",
				radius: 20,
			}),
			new TrajectoryCircleStroke(geom1, {
				mcoords: mValues1,
				colour: "red",
				radius: 24,
				width: 10,
			}),
			new Hair(geom1, { colour: "black" }),
		]);

		const acetFill = platina.getAcetateOfClass(TrajectoryCircleFill.Acetate);
		const acetStrk = platina.getAcetateOfClass(TrajectoryCircleStroke.Acetate);

		acetFill.mValue = acetStrk.mValue = 275;

		await platina.once("render");
		await expectPixelmatch(
			context,
			"spec/09-trajectorify/trajectorify-circles-0275",
			100,
			0
		);

		acetFill.mValue = acetStrk.mValue = 832;

		await platina.once("render");
		await expectPixelmatch(
			context,
			"spec/09-trajectorify/trajectorify-circles-0832",
			100,
			0
		);

		acetFill.mValue = acetStrk.mValue = 1555;

		await platina.once("render");
		await expectPixelmatch(
			context,
			"spec/09-trajectorify/trajectorify-circles-1555",
			100,
			0
		);

		acetFill.mValue = acetStrk.mValue = 2310;

		await platina.once("render");
		await expectPixelmatch(
			context,
			"spec/09-trajectorify/trajectorify-circles-2310",
			100,
			0
		);
	});
});
