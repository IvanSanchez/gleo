import Geometry from "../../src/geometry/Geometry.mjs";
import Platina from "../../src/Platina.mjs";
import cartesian from "../../src/crs/cartesian.mjs";
import TintedSprite from "../../src/symbols/TintedSprite.mjs";
import { setFactory } from "../../src/geometry/DefaultGeometry.mjs";
import desaturatedMarker from "./desaturatedMarker.mjs";
import ivansFace from "./ivansFace.mjs";

let headless = false;
try {
	window;
} catch (ex) {
	headless = true;
}

// These tests seem to time out in CI servers more often than any other test.
jasmine.DEFAULT_TIMEOUT_INTERVAL = 20000;

setFactory(function cartesianize(coords) {
	return new Geometry(cartesian, coords);
});

function spawnPlatina() {
	const context = contextFactory(256, 256, { preserveDrawingBuffer: true });
	return [
		new Platina(context, {
			crs: cartesian,
			center: [0, 0],
			scale: 2,
			preserveDrawingBuffer: true,
		}),
		context,
	];
}

describe("TintedSprite", function () {
	it("Single", async function () {
		if (headless) {
			pending(
				"Headless environment lacks support for either HTMLImageElement or ImageData"
			);
		}

		const [platina, context] = spawnPlatina();

		new TintedSprite([10, 0], {
			image: desaturatedMarker,
			// spriteSize: [26,41],
			spriteAnchor: [13, 41],
			tint: "red",
		}).addTo(platina);

		await new Promise((res) =>
			platina.getAcetateOfClass(TintedSprite.Acetate).on("symbolsadded", res)
		);

		await platina.once("render");

		await expectPixelmatch(
			context,
			"spec/08-tinted-sprite/tinted-sprite-single",
			20,
			0
		);
	});

	it("Multiple", async function () {
		if (headless) {
			pending(
				"Headless environment lacks support for either HTMLImageElement or ImageData"
			);
		}
		const [platina, context] = spawnPlatina();

		const spriteOpts = {
			image: desaturatedMarker,
			spriteAnchor: [13, 41],
		};

		platina.multiAdd([
			new TintedSprite([-120, 0], { ...spriteOpts, tint: "red" }),
			new TintedSprite([-80, 0], { ...spriteOpts, tint: "orange" }),
			new TintedSprite([-40, 0], { ...spriteOpts, tint: "yellow" }),
			new TintedSprite([0, 0], { ...spriteOpts, tint: "green" }),
			new TintedSprite([40, 0], { ...spriteOpts, tint: "cyan" }),
			new TintedSprite([80, 0], { ...spriteOpts, tint: "blue" }),
			new TintedSprite([120, 0], { ...spriteOpts, tint: "purple" }),
		]);

		await new Promise((res) =>
			platina.getAcetateOfClass(TintedSprite.Acetate).on("symbolsadded", res)
		);

		await platina.once("render");

		await expectPixelmatch(
			context,
			"spec/08-tinted-sprite/tinted-sprite-multple",
			100,
			0
		);
	});

	it("Multiple, retint", async function () {
		if (headless) {
			pending(
				"Headless environment lacks support for either HTMLImageElement or ImageData"
			);
		}
		const [platina, context] = spawnPlatina();

		const spriteOpts = {
			image: desaturatedMarker,
			spriteAnchor: [13, 41],
		};

		const sprites = [
			new TintedSprite([-120, 0], { ...spriteOpts, tint: "red" }),
			new TintedSprite([-80, 0], { ...spriteOpts, tint: "orange" }),
			new TintedSprite([-40, 0], { ...spriteOpts, tint: "yellow" }),
			new TintedSprite([0, 0], { ...spriteOpts, tint: "green" }),
			new TintedSprite([40, 0], { ...spriteOpts, tint: "cyan" }),
			new TintedSprite([80, 0], { ...spriteOpts, tint: "blue" }),
			new TintedSprite([120, 0], { ...spriteOpts, tint: "purple" }),
		];

		platina.multiAdd(sprites);

		await new Promise((res) =>
			platina.getAcetateOfClass(TintedSprite.Acetate).on("symbolsadded", res)
		);

		await platina.once("render");

		await expectPixelmatch(
			context,
			"spec/08-tinted-sprite/tinted-sprite-multple",
			100,
			0
		);

		sprites[0].tint = "black";
		sprites[1].tint = "white";
		sprites[2].tint = "purple";
		sprites[3].tint = "indigo";
		sprites[4].tint = "brown";
		sprites[5].tint = "black";
		sprites[6].tint = "white";

		await platina.once("render");

		await expectPixelmatch(
			context,
			"spec/08-tinted-sprite/tinted-sprite-multple-retint",
			100,
			0
		);
	});

	it("Flower", async function () {
		if (headless) {
			pending(
				"Headless environment lacks support for either HTMLImageElement or ImageData"
			);
		}

		const [platina, context] = spawnPlatina();

		const spriteOpts = {
			image: desaturatedMarker,
			spriteAnchor: [13, 41],
		};
		let red = new TintedSprite([0, 0], {
			...spriteOpts,
			spriteScale: 2,
			tint: "red",
		});
		platina.multiAdd([
			red,
			new TintedSprite([0, 0], { ...spriteOpts, yaw: 50, tint: "orange" }),
			new TintedSprite([0, 0], { ...spriteOpts, yaw: 100, tint: "yellow" }),
			new TintedSprite([0, 0], { ...spriteOpts, yaw: 150, tint: "green" }),
			new TintedSprite([0, 0], { ...spriteOpts, yaw: 200, tint: "cyan" }),
			new TintedSprite([0, 0], { ...spriteOpts, yaw: 250, tint: "blue" }),
			new TintedSprite([0, 0], {
				...spriteOpts,
				yaw: 300,
				spriteScale: 0.5,
				tint: "purple",
			}),
		]);
		await platina.once("render");
		await new Promise(requestAnimationFrame);

		await expectPixelmatch(
			context,
			"spec/08-tinted-sprite/tinted-sprite-flower",
			100,
			0
		);

		red.yaw = 180;
		await platina.once("render");

		await expectPixelmatch(
			context,
			"spec/08-tinted-sprite/tinted-sprite-flower-2",
			100,
			0
		);

		red.spriteScale = 4;
		await platina.once("render");

		await expectPixelmatch(
			context,
			"spec/08-tinted-sprite/tinted-sprite-flower-3",
			100,
			0
		);
	});

	it("Image replacement", async function () {
		if (headless) {
			pending(
				"Headless environment lacks support for either HTMLImageElement or ImageData"
			);
		}

		const [platina, context] = spawnPlatina();

		let sprite = new TintedSprite([10, 0], {
			image: desaturatedMarker,
			// spriteSize: [26,41],
			spriteAnchor: [13, 41],
			tint: "red",
		}).addTo(platina);

		await new Promise((res) =>
			platina.getAcetateOfClass(TintedSprite.Acetate).on("symbolsadded", res)
		);

		await platina.once("render");

		await expectPixelmatch(
			context,
			"spec/08-tinted-sprite/tinted-sprite-single",
			20,
			0
		);

		sprite.tint = "yellow";
		await sprite.replaceImage({
			image: ivansFace,
			spriteAnchor: ["50%", "50%"],
			spriteSize: undefined,
		});

		await platina.once("render");

		await expectPixelmatch(
			context,
			"spec/08-tinted-sprite/tinted-sprite-yellow-face",
			20,
			0
		);

		sprite.tint = "green";
		await sprite.replaceImage({
			image: desaturatedMarker,
			spriteSize: undefined,
			spriteAnchor: [13, 41],
		});

		await platina.once("render");

		await expectPixelmatch(
			context,
			"spec/08-tinted-sprite/tinted-sprite-single-green",
			20,
			0
		);
	});
});
