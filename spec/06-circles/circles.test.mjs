import Geometry from "../../src/geometry/Geometry.mjs";
import Platina from "../../src/Platina.mjs";
import cartesian from "../../src/crs/cartesian.mjs";
import CircleFill from "../../src/symbols/CircleFill.mjs";
import CircleStroke from "../../src/symbols/CircleStroke.mjs";
import { setFactory } from "../../src/geometry/DefaultGeometry.mjs";

setFactory(function cartesianize(coords) {
	return new Geometry(cartesian, coords);
});

function spawnPlatina() {
	const context = contextFactory(384, 384, { preserveDrawingBuffer: true });
	return [
		new Platina(context, {
			crs: cartesian,
			center: [0, 0],
			scale: 0.75,
			preserveDrawingBuffer: true,
		}),
		context,
	];
}

describe("Circles", function () {
	it("Strokes, single", async function () {
		const [platina, context] = spawnPlatina();

		new CircleStroke([-50, -70], { radius: 50, width: 10, colour: "red" }).addTo(
			platina
		);
		new CircleStroke([50, -70], {}).addTo(platina);
		new CircleStroke([50, -70], { offset: [0, 10], colour: "green", width: 5 }).addTo(
			platina
		);
		new CircleStroke([50, -70], {
			offset: [0, 20],
			colour: "darkgreen",
			width: 8,
		}).addTo(platina);
		new CircleStroke([-50, 70], { radius: 50, width: 20, colour: "blue" }).addTo(
			platina
		);

		await platina.once("render");

		await expectPixelmatch(context, "spec/06-circles/circles-strokes-single", 100, 0);
	});

	it("Strokes, multiAdd", async function () {
		const [platina, context] = spawnPlatina();

		platina.multiAdd([
			new CircleStroke([-50, -70], { radius: 50, width: 10, colour: "red" }),
			new CircleStroke([50, -70], {}),
			new CircleStroke([50, -70], { offset: [0, 10], colour: "green", width: 5 }),
			new CircleStroke([50, -70], {
				offset: [0, 20],
				colour: "darkgreen",
				width: 8,
			}),
			new CircleStroke([-50, 70], { radius: 50, width: 20, colour: "blue" }),
		]);

		await platina.once("render");

		await expectPixelmatch(
			context,
			"spec/06-circles/circles-strokes-multiadd",
			100,
			0
		);
	});

	it("Fills", async function () {
		const [platina, context] = spawnPlatina();

		platina.multiAdd([
			new CircleFill([70, 90], { colour: "lime" }),
			new CircleFill([70, 70], { radius: 20, colour: "red" }),
			new CircleFill([50, 90], { offset: [0, 10], colour: "purple" }),
			new CircleFill([50, 40], { offset: [0, 20], colour: "black" }),
			new CircleFill([50, 0], { radius: 50, colour: "pink" }),
		]);

		await platina.once("render");

		await expectPixelmatch(context, "spec/06-circles/circles-fills", 100, 0);
	});

	it("Strokes and fills", async function () {
		const [platina, context] = spawnPlatina();

		platina.multiAdd([
			new CircleStroke([-50, -70], { radius: 50, width: 10, colour: "red" }),
			new CircleStroke([50, -70], {}),
			new CircleStroke([50, -70], { offset: [0, 10], colour: "green", width: 5 }),
			new CircleStroke([50, -70], {
				offset: [0, 20],
				colour: "darkgreen",
				width: 8,
			}),
			new CircleStroke([-50, 70], { radius: 50, width: 20, colour: "blue" }),
			new CircleFill([70, 90], { colour: "lime" }),
			new CircleFill([70, 70], { radius: 20, colour: "red" }),
			new CircleFill([50, 90], { offset: [0, 10], colour: "purple" }),
			new CircleFill([50, 40], { offset: [0, 20], colour: "black" }),
			new CircleFill([50, 0], { radius: 50, colour: "pink" }),
		]);

		await platina.once("render");

		await expectPixelmatch(context, "spec/06-circles/circles-complete", 100, 0);
	});
});
