import Geometry from "../../src/geometry/Geometry.mjs";
import Platina from "../../src/Platina.mjs";
import Pie from "../../src/symbols/Pie.mjs";
import CircleFill from "../../src/symbols/CircleFill.mjs";
import cartesian from "../../src/crs/cartesian.mjs";
import Clusterer from "../../src/loaders/Clusterer.mjs";
import SymbolGroup from "../../src/loaders/SymbolGroup.mjs";

function spawnPlatina() {
	const context = contextFactory(256, 256, { preserveDrawingBuffer: true });
	return [
		new Platina(context, {
			crs: cartesian,
			center: new Geometry(cartesian, [0, 0]),
			scale: 0.1,
			preserveDrawingBuffer: true,
			backgroundColour: [255, 255, 255, 255],
		}),
		context,
	];
}

// Seedable pseudo-random function, from https://stackoverflow.com/questions/521295/seeding-the-random-number-generator-in-javascript
function sfc32(a, b, c, d) {
	return function () {
		a >>>= 0;
		b >>>= 0;
		c >>>= 0;
		d >>>= 0;
		var t = (a + b) | 0;
		a = b ^ (b >>> 9);
		b = (c + (c << 3)) | 0;
		c = (c << 21) | (c >>> 11);
		d = (d + 1) | 0;
		t = (t + d) | 0;
		c = (c + t) | 0;
		return (t >>> 0) / 4294967296;
	};
}

const pseudorandom = sfc32(1111, 2222, 3333, 4444);

// Generate a bunch of pseudo-random coordinates
const points = Array(100)
	.fill(0)
	.map(() => [pseudorandom() * 20 - 10, pseudorandom() * 20 - 10]);

function clusterSymbolizer(symbols) {
	const colours = {};
	symbols.forEach((s) => {
		const c = s.colour[2] == 255 ? "blue" : "green";
		if (!colours[c]) {
			colours[c] = 1;
		} else {
			colours[c] += 1;
		}
	});

	return [
		new Pie(symbols[0].geometry, {
			radius: 10 + Math.log2(symbols.length) * 2,
			slices: colours,
		}),
	];
}

describe(`Cluster`, function () {
	it(`static pies`, async function () {
		const [platina, context] = spawnPlatina();

		let blueCircles = points.slice(0, 25).map(
			(c) =>
				new CircleFill(new Geometry(cartesian, c), {
					colour: "blue",
					radius: 5,
				})
		);

		let greenCircles = points.slice(25, 60).map((c) => {
			return new CircleFill(new Geometry(cartesian, c), {
				colour: "green",
				radius: 5,
			});
		});

		const clusterer = new Clusterer({
			clusterSetFactor: 1,
			distance: 50,
			scaleLimit: 0.01,
			clusterSymbolizer: clusterSymbolizer,
		}).addTo(platina);

		// clusterer.on("reset", ()=>console.log("clusterer reset"))
		// clusterer.on("build", (ev)=>console.log("clusterer built level",ev.detail))

		clusterer.multiAdd(blueCircles.concat(greenCircles));

		// console.log(clusterer);

		await platina.once(`render`);
		await expectPixelmatch(context, `spec/19-cluster/green-blue`, 10, 10);

		clusterer.multiRemove(blueCircles);

		await new Promise(requestAnimationFrame); // Needed for waiting until r-bush recalculation
		await platina.once(`render`);

		return await expectPixelmatch(context, `spec/19-cluster/only-green`, 10, 10);
	});

	it(`nested symbolgroups and multisymbols`, async function () {
		const [platina, context] = spawnPlatina();

		let blueCircles = points.slice(0, 25).map(
			(c) =>
				new CircleFill(new Geometry(cartesian, c), {
					colour: "blue",
					radius: 5,
				})
		);

		let greenCircles = points.slice(25, 60).map((c) => {
			return new CircleFill(new Geometry(cartesian, c), {
				colour: "green",
				radius: 5,
			});
		});

		const blueGroup = new SymbolGroup().multiAdd(blueCircles);
		const greenGroup = new SymbolGroup().multiAdd(greenCircles);

		const clusterer = new Clusterer({
			clusterSetFactor: 1,
			distance: 50,
			scaleLimit: 0.01,
			clusterSymbolizer: clusterSymbolizer,
		}).addTo(platina);

		// clusterer.on("reset", () => console.log("clusterer reset"));
		// clusterer.on("build", (ev) => console.log("clusterer built level", ev.detail));

		// clusterer.multiAdd(blueCircles.concat(greenCircles));
		blueGroup.addTo(clusterer);
		greenGroup.addTo(clusterer);

		await platina.once(`render`);
		await expectPixelmatch(context, `spec/19-cluster/green-blue`, 10, 10);

		expect(clusterer.has(blueGroup)).toBeTrue();
		expect(clusterer.has(blueCircles[0])).toBeTrue();
		expect(clusterer.has(greenGroup)).toBeTrue();
		expect(clusterer.has(greenCircles[0])).toBeTrue();

		// Remove the blue group
		clusterer.remove(blueGroup);

		await clusterer.once("build");
		await platina.once(`render`);
		await expectPixelmatch(context, `spec/19-cluster/only-green`, 10, 10);

		expect(clusterer.has(blueGroup)).toBeFalse();
		expect(clusterer.has(greenGroup)).toBeTrue();

		// Add the blue circles to the remaining group
		greenGroup.multiAdd(blueCircles);

		await clusterer.once("build");
		await platina.once(`render`);
		await expectPixelmatch(context, `spec/19-cluster/green-blue-2`, 10, 10);

		expect(clusterer.has(blueGroup)).toBeFalse();
		expect(clusterer.has(blueCircles[0])).toBeTrue();
		expect(clusterer.has(greenGroup)).toBeTrue();

		// Remove circles from the remaining group
		greenGroup.multiRemove(greenCircles);

		expect(clusterer.has(blueGroup)).toBeFalse();
		expect(clusterer.has(blueCircles[0])).toBeTrue();
		expect(clusterer.has(greenGroup)).toBeTrue();
		expect(clusterer.has(greenCircles[0])).toBeFalse();

		await clusterer.once("build");
		await platina.once(`render`);
		await expectPixelmatch(context, `spec/19-cluster/only-blue`, 10, 10);
	});

	it(`cluster inside symbolgroup, remove groups`, async function () {
		const [platina, context] = spawnPlatina();

		let blueCircles = points.slice(0, 25).map(
			(c) =>
				new CircleFill(new Geometry(cartesian, c), {
					colour: "blue",
					radius: 5,
				})
		);

		let greenCircles = points.slice(25, 60).map(
			(c) =>
				new CircleFill(new Geometry(cartesian, c), {
					colour: "green",
					radius: 5,
				})
		);

		const blueGroup = new SymbolGroup();
		const greenGroup = new SymbolGroup();

		const blueClusterer = new Clusterer({
			clusterSetFactor: 1,
			distance: 50,
			scaleLimit: 0.01,
			clusterSymbolizer: clusterSymbolizer,
		}).addTo(blueGroup);

		const greenClusterer = new Clusterer({
			clusterSetFactor: 1,
			distance: 50,
			scaleLimit: 0.01,
			clusterSymbolizer: clusterSymbolizer,
		});

		expect(blueGroup.has(blueClusterer)).toBeTrue();
		// expect(greenGroup.has(greenClusterer)).toBeTrue();

		blueClusterer.multiAdd(blueCircles);

		expect(platina.has(blueGroup)).toBeFalse();
		expect(platina.has(greenGroup)).toBeFalse();

		blueGroup.addTo(platina);
		// greenGroup.addTo(platina);

		await platina.once(`render`);
		await expectPixelmatch(context, `spec/19-cluster/only-blue`, 10, 10);

		expect(platina.has(blueGroup)).toBeTrue();
		expect(platina.has(greenGroup)).toBeFalse();

		platina.add(greenGroup);
		greenGroup.add(greenClusterer);

		greenClusterer.multiAdd(greenCircles);

		await greenClusterer.once("build");
		await platina.once(`render`);
		await expectPixelmatch(context, `spec/19-cluster/greens-over-blues`, 10, 10);

		expect(platina.has(blueGroup)).toBeTrue();
		expect(platina.has(greenGroup)).toBeTrue();

		blueGroup.remove();

		await platina.once(`render`);
		await expectPixelmatch(context, `spec/19-cluster/only-green`, 10, 10);

		blueGroup.addTo(platina);

		await platina.once(`render`);
		await expectPixelmatch(context, `spec/19-cluster/greens-over-blues`, 10, 10);

		blueClusterer.remove();

		await platina.once(`render`);
		await expectPixelmatch(context, `spec/19-cluster/only-green`, 10, 10);
	});

	it(`cluster inside symbolgroup, remove cluster and re-add group`, async function () {
		// This ensures that there are no lingering references to symbols in
		// the group when a nested clusterer is removed.

		const [platina, context] = spawnPlatina();

		let circles = points.slice(0, 25).map(
			(c) =>
				new CircleFill(new Geometry(cartesian, c), {
					colour: "blue",
					radius: 5,
				})
		);

		const group = new SymbolGroup();

		const clusterer = new Clusterer({
			clusterSetFactor: 1,
			distance: 50,
			scaleLimit: 0.01,
			clusterSymbolizer: clusterSymbolizer,
		});

		clusterer.multiAdd(circles);

		group.add(clusterer);

		platina.add(group);

		expect(platina.has(group)).toBeTrue();
		expect(platina.has(clusterer)).toBeFalse();
		expect(platina.has(circles[0])).toBeTrue(); // The 0th circle is not clustered
		expect(platina.has(circles[2])).toBeFalse(); // The 2nd circle is clustered

		await platina.once(`render`);
		await expectPixelmatch(context, `spec/19-cluster/only-blue`, 10, 10);

		group.remove(clusterer);

		expect(group.symbols.length).toEqual(0);

		platina.remove(group);

		expect(platina.has(group)).toBeFalse();

		platina.add(group);

		expect(platina.has(group)).toBeTrue();
	});
});
