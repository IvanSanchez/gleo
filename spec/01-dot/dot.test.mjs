import Geometry from "../../src/geometry/Geometry.mjs";
import Platina from "../../src/Platina.mjs";
import Dot from "../../src/symbols/Dot.mjs";
import cartesian from "../../src/crs/cartesian.mjs";
import epsg4326 from "../../src/crs/epsg4326.mjs";
import LatLng from "../../src/geometry/LatLng.mjs";

describe("One dot", function () {
	it("cartesian, platina options", async function () {
		const context = contextFactory(256, 256, { preserveDrawingBuffer: true });
		const platina = new Platina(context, {
			crs: cartesian,
			center: new Geometry(cartesian, [0, 0]),
			scale: 0.02,
			preserveDrawingBuffer: true,
		});

		let dot1 = new Dot(new Geometry(cartesian, [0, 0]), {
			colour: "purple",
			size: 25,
		});

		platina.multiAdd([dot1]);

		await platina.once("render");

		// This one test renders differently on GPUs - the 25x25 square
		// might be one pixel off horizontally.
		return expectPixelmatch(context, "spec/01-dot/dot-1-purple", 50, 50);
	});

	it("cartesian, platina setters", async function () {
		const context = contextFactory(256, 256, { preserveDrawingBuffer: true });
		const platina = new Platina(context);

		let dot1 = new Dot(new Geometry(cartesian, [0, 0]), {
			colour: "green",
			size: 25,
		}).addTo(platina);

		platina.crs = cartesian;
		platina.setView({
			center: new Geometry(cartesian, [0, 0]),
			scale: 0.02,
		});

		await platina.once("render");

		return expectPixelmatch(context, "spec/01-dot/dot-1-green", 50, 50);
	});

	it("latlng, platina options", async function () {
		const context = contextFactory(256, 256, { preserveDrawingBuffer: true });
		const platina = new Platina(context, {
			crs: epsg4326,
			center: new LatLng([40, -120]),
			scale: 0.01,
		});

		let dot1 = new Dot(new LatLng([39.5, -120.5]), {
			colour: "blue",
			size: 25,
		}).addTo(platina);

		await platina.once("render");
		return expectPixelmatch(context, "spec/01-dot/dot-1-blue", 50, 50);
	});
});
