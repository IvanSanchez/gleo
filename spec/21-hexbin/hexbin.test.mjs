import Geometry from "../../src/geometry/Geometry.mjs";
import Platina from "../../src/Platina.mjs";
import intensify from "../../src/symboldecorators/intensify.mjs";
import trajectorify from "../../src/symboldecorators/trajectorify.mjs";
// import CircleFill from "../../src/symbols/CircleFill.mjs";
import Hair from "../../src/symbols/Hair.mjs";
// import HeatPoint from "../../src/symbols/HeatPoint.mjs";
import VertexDot from "../../src/symbols/VertexDot.mjs";
import cartesian from "../../src/crs/cartesian.mjs";
import HexBin from "../../src/fields/HexBin.mjs";

let headless = false;
try {
	window;
} catch (ex) {
	headless = true;
}

function spawnPlatina() {
	const context = contextFactory(384, 384, { preserveDrawingBuffer: true });
	return [
		new Platina(context, {
			crs: cartesian,
			center: new Geometry(cartesian, [0.2, 0.6]),
			scale: 0.002,
			preserveDrawingBuffer: true,
			backgroundColour: "white",
		}),
		context,
	];
}

describe("hexbin", function () {
	it(`on trajectorified intensified VertexDots`, async function () {
		if (headless) {
			pending("Headless environment lacks render-to-float32-textures support");
		}
		const [platina, context] = spawnPlatina();

		const field = new HexBin(platina, {
			stops: {
				0: [0, 0, 0, 0],
				10: [0, 0, 255, 128],
				25: [0, 255, 255, 128],
				50: [255, 255, 0, 128],
				75: [255, 0, 0, 128],
			},
			zIndex: 2000,
			cellSize: 64,
			marginSize: 8,
		});

		const HexDot = trajectorify(intensify(VertexDot));
		const TrajDot = trajectorify(VertexDot);

		const trajIntAcetate = new HexDot.Acetate(field);
		const trajAcetate = new TrajDot.Acetate(platina);

		const dot1 = new HexDot(
			new Geometry(cartesian, [
				[0, 0.5],
				[0.22, 0.6],
				[0.22, 0.8],
			]),
			{ mcoords: [0, 100, 200], intensity: 10 }
		).addTo(trajIntAcetate);

		const dot2 = new HexDot(
			new Geometry(cartesian, [
				[0.1, 0.6],
				[0.22, 0.7],
				[0.3, 0.55],
			]),
			{ mcoords: [0, 100, 200], intensity: 25 }
		);

		const dot3 = new HexDot(
			new Geometry(cartesian, [
				[0.3, 0.5],
				[0.1, 0.5],
				[0.4, 0.7],
			]),
			{ mcoords: [0, 100, 200], intensity: 15 }
		);

		const hair1 = new Hair(dot1.geometry).addTo(platina);
		platina.multiAdd([dot2, dot3, new Hair(dot2.geometry), new Hair(dot3.geometry)]);

		platina.multiAdd([
			new TrajDot(dot1.geometry, { mcoords: dot1.mcoords, size: 16 }),
			new TrajDot(dot2.geometry, { mcoords: dot2.mcoords, size: 16 }),
			new TrajDot(dot3.geometry, { mcoords: dot3.mcoords, size: 16 }),
		]);

		trajIntAcetate.mValue = trajAcetate.mValue = 0;

		await platina.once("render");
		await expectPixelmatch(context, `spec/21-hexbin/hexbin1`, 3, 0);

		trajIntAcetate.mValue = trajAcetate.mValue = 90;

		await platina.once("render");
		await expectPixelmatch(context, `spec/21-hexbin/hexbin2`, 14, 0);

		trajIntAcetate.mValue = trajAcetate.mValue = 140;

		await Promise.all([new Promise(requestAnimationFrame), platina.once("render")]);
		await expectPixelmatch(context, `spec/21-hexbin/hexbin3`, 14, 0);
	});
});
