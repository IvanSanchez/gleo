import Geometry from "../../src/geometry/Geometry.mjs";
import Platina from "../../src/Platina.mjs";
import Fill from "../../src/symbols/Fill.mjs";
import cartesian from "../../src/crs/cartesian.mjs";
import { setFactory } from "../../src/geometry/DefaultGeometry.mjs";

setFactory(function cartesianize(coords) {
	return new Geometry(cartesian, coords);
});

function spawnPlatina() {
	const context = contextFactory(256, 256, { preserveDrawingBuffer: true });
	return [
		new Platina(context, {
			crs: cartesian,
			center: new Geometry(cartesian, [0, 0]),
			scale: 0.1,
			preserveDrawingBuffer: true,
		}),
		context,
	];
}

describe("Polygon fill", function () {
	it("Triangle, closed", async function () {
		const [platina, context] = spawnPlatina();

		new Fill(
			[
				[-10, -10],
				[-5, 8],
				[0, -5],
				[-10, -10],
			],
			{
				colour: "black",
			}
		).addTo(platina);

		await platina.once("render");
		return await expectPixelmatch(context, "spec/10-polygon-fill/triangle", 10, 10);
	});

	it("Triangle, unclosed", async function () {
		const [platina, context] = spawnPlatina();

		new Fill(
			[
				[-10, -10],
				[-5, 8],
				[0, -5],
			],
			{
				colour: "black",
			}
		).addTo(platina);

		await platina.once("render");
		return await expectPixelmatch(context, "spec/10-polygon-fill/triangle", 10, 10);
	});

	it("Concave, closed", async function () {
		const [platina, context] = spawnPlatina();

		let concave = new Fill(
			[
				[-10, -10],
				[-5, 8],
				[0, -5],
				[5, 10],
				[10, -10],
				[-10, -10],
			],
			{
				colour: "black",
			}
		).addTo(platina);

		await platina.once("render");
		await expectPixelmatch(context, "spec/10-polygon-fill/concave-black", 10, 10);

		concave.colour = "red";

		await platina.once("render");
		return await expectPixelmatch(
			context,
			"spec/10-polygon-fill/concave-red",
			10,
			10
		);
	});

	it("Concave, unclosed", async function () {
		const [platina, context] = spawnPlatina();

		let concave = new Fill(
			[
				[-10, -10],
				[-5, 8],
				[0, -5],
				[5, 10],
				[10, -10],
			],
			{
				colour: "black",
			}
		).addTo(platina);

		await platina.once("render");
		await expectPixelmatch(context, "spec/10-polygon-fill/concave-black", 10, 10);

		concave.colour = "red";

		await platina.once("render");
		return await expectPixelmatch(
			context,
			"spec/10-polygon-fill/concave-red",
			10,
			10
		);
	});

	it("Concave, update geometry", async function () {
		const [platina, context] = spawnPlatina();

		let concave = new Fill(
			[
				[-10, -10],
				[-5, 8],
				[0, -5],
				[5, 10],
				[10, -10],
			],
			{
				colour: "black",
			}
		).addTo(platina);

		await platina.once("render");
		await expectPixelmatch(context, "spec/10-polygon-fill/concave-black", 10, 10);

		concave.geometry = [
			[-10, -10],
			[-5, 8],
			[0, 2],
			[5, 11],

			[10, -12],
			[4, -2],
			[0, -10],
			[-10, -10],
		];

		await platina.once("render");
		return await expectPixelmatch(
			context,
			"spec/10-polygon-fill/concave-updated",
			10,
			10
		);
	});
});
