import Geometry from "../../src/geometry/Geometry.mjs";
import Platina from "../../src/Platina.mjs";
import StrokeRoad from "../../src/symbols/StrokeRoad.mjs";
import cartesian from "../../src/crs/cartesian.mjs";
import trailify from "../../src/symboldecorators/trailify.mjs";

function spawnPlatina() {
	const context = contextFactory(256, 256, { preserveDrawingBuffer: true });
	return [
		new Platina(context, {
			crs: cartesian,
			center: new Geometry(cartesian, [0, 0]),
			scale: 0.1,
			preserveDrawingBuffer: true,
			backgroundColour: [255, 255, 255, 255],
		}),
		context,
	];
}

describe(`StrokeRoad`, function () {
	it(`zig-zag`, async function () {
		const [platina, context] = spawnPlatina();

		new StrokeRoad(
			new Geometry(cartesian, [
				[-10, -10],
				[-5, 8],
				[0, -5],
				[5, 10],
				[10, -10],
			]),
			{
				outColour: `black`,
				outWidth: 8,
				colour: `green`,
				width: 24,
			}
		).addTo(platina);

		await platina.once(`render`);
		return expectPixelmatch(context, `spec/18-strokeroad/zigzag`, 13, 10);
	});
});

describe(`Trailified StrokeRoad`, function () {
	it(`zig-zag 1`, async function () {
		const [platina, context] = spawnPlatina();

		const TrailRoad = trailify(StrokeRoad);

		new TrailRoad(
			new Geometry(cartesian, [
				[-10, -10],
				[-5, 8],
				[0, -5],
				[5, 10],
				[10, -10],
			]),
			{
				mcoords: [100, 500, 900, 1400, 1800],
				outColour: `blue`,
				outWidth: 16,
				colour: `green`,
				width: 32,
			}
		).addTo(platina);

		await platina.once(`render`);

		const acetTrail = platina.getAcetateOfClass(TrailRoad.Acetate);

		acetTrail.minMValue = 200;
		acetTrail.maxMValue = 800;

		await platina.once(`render`);
		await expectPixelmatch(context, `spec/18-strokeroad/zigzag-trail-1`, 10, 10);

		acetTrail.minMValue = 600;
		acetTrail.maxMValue = 1500;

		await platina.once(`render`);
		await expectPixelmatch(context, `spec/18-strokeroad/zigzag-trail-2`, 10, 10);
	});
});
