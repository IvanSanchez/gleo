import Geometry from "../../src/geometry/Geometry.mjs";
import Platina from "../../src/Platina.mjs";
// import Stroke from "../../src/symbols/Stroke.mjs";
// import Dot from "../../src/symbols/Dot.mjs";
import epsg3857 from "../../src/crs/epsg3857.mjs";
import LatLng from "../../src/geometry/LatLng.mjs";
import RasterTileLoader from "../../src/loaders/RasterTileLoader.mjs";
import create3857Pyramid from "../../src/geometry/Pyramid3857.mjs";

let headless = false;
try {
	window;
} catch (ex) {
	headless = true;
}

function spawnPlatina() {
	const context = contextFactory(384, 384, { preserveDrawingBuffer: true });
	return [
		new Platina(context, {
			crs: epsg3857,
			center: new LatLng([0, 0]),
			scale: 78926.33918,
			preserveDrawingBuffer: true,
		}),
		context,
	];
}

const tileSize = 128;

// const canvas = document.createElement("canvas");
// canvas.setAttribute("width", tileSize);
// canvas.setAttribute("height", tileSize);

const canvas = contextFactory2d(tileSize, tileSize);
const ctx = canvas.getContext("2d");
ctx.imageSmoothingEnabled = false;
ctx.lineWidth = 1;
ctx.font = tileSize / 8 + "px DejaVu Sans";
ctx.textAlign = "center";

// if (typeof document !== undefined) {
// 	document.body.appendChild(canvas);
// }

function getImage(z, x, y, controller) {
	//console.log("requested", z, x, y);
	return new Promise((res, rej) => {
		const timeout = setTimeout(() => {
			ctx.clearRect(0, 0, tileSize, tileSize);

			if ((x + y) % 2 !== 0) {
				ctx.fillStyle = "rgba(230,255,230,1)";
				ctx.fillRect(0, 0, tileSize, tileSize);
				ctx.strokeStyle = "green";
				ctx.fillStyle = "green";
			} else {
				ctx.fillStyle = "rgba(255,230,230,1)";
				ctx.fillRect(0, 0, tileSize, tileSize);
				ctx.strokeStyle = "red";
				ctx.fillStyle = "red";
			}

			ctx.strokeRect(1.5, 1.5, tileSize - 3, tileSize - 3);

			ctx.fillText(`${z}: ${x},${y}`, tileSize / 2, (tileSize * 3) / 4);

			// return tile;
			//console.log("ready", z, x, y);

			// For graphical browsers, a `res(canvas)` just works.
			// But the headless implementation doesn't.
			// 			return res(canvas);
			return res(ctx.getImageData(0, 0, tileSize, tileSize));
		}, 15);

		controller.signal.addEventListener("abort", () => {
			// 			console.log("aborted", z, x, y);
			rej(clearTimeout(timeout));
		});
	});
}

const rasterTiles = new RasterTileLoader(create3857Pyramid(0, 12, 128), getImage, {
	tileResX: tileSize,
	tileResY: tileSize,
	fadeInDuration: 0,
});

describe("Synthetic mercator raster tiles", function () {
	it("one zoom-0 tile", async function () {
		// if (headless) {
		pending("Raster tile tests disabled");
		// }

		const [platina, context] = spawnPlatina();

		platina.scale = 78926.33918 * 2;

		rasterTiles.addTo(platina);
		const tileload = new Promise((res) => rasterTiles.on("tileload", res));
		platina.scale = 78926.33918 * 4;
		platina.center = new LatLng([0, 0]);

		await tileload;
		await platina.once("render");

		/// FIXME: This test causes 0 different pixels in most platforms,
		/// except chromium headless in a gitlab CI runner. It does work
		/// on headless chromium on a dev machine. WHY?!
		await expectPixelmatch(context, "spec/04-raster-tiles/z0", 3, 0);
	});

	it("zoom-1 tiles", async function () {
		// if (headless) {
		pending("Raster tile tests disabled");
		// }
		const [platina, context] = spawnPlatina();

		rasterTiles.addTo(platina);
		platina.scale = 78926.33918 * 2;
		platina.center = new LatLng([0, 0]);

		const tileload = new Promise((res) => {
			let done = 0;
			rasterTiles.on("tileload", () => {
				if (++done >= 4) res();
			});
		});
		await tileload;
		await platina.once("render");

		await expectPixelmatch(context, "spec/04-raster-tiles/z1", 8, 0);
	});
});
