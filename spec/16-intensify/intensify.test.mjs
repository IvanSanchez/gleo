import Geometry from "../../src/geometry/Geometry.mjs";
import Platina from "../../src/Platina.mjs";
import intensify from "../../src/symboldecorators/intensify.mjs";
import CircleFill from "../../src/symbols/CircleFill.mjs";
import HeatPoint from "../../src/symbols/HeatPoint.mjs";
import cartesian from "../../src/crs/cartesian.mjs";
import HeatMap from "../../src/fields/HeatMap.mjs";

let headless = false;
try {
	window;
} catch (ex) {
	headless = true;
}

function spawnPlatina() {
	const context = contextFactory(384, 384, { preserveDrawingBuffer: true });
	return [
		new Platina(context, {
			crs: cartesian,
			center: [0, 0],
			scale: 0.01,
			preserveDrawingBuffer: true,
			backgroundColour: "white",
		}),
		context,
	];
}

describe("intensify", function () {
	it(`circles on heatmap`, async function () {
		if (headless) {
			pending("Headless environment lacks render-to-float32-textures support");
		}
		const [platina, context] = spawnPlatina();

		const field = new HeatMap(platina, {
			stops: {
				0: [0, 0, 0, 0],
				10: [0, 0, 255, 255],
				100: [0, 255, 255, 255],
				200: [255, 255, 0, 255],
				300: [255, 0, 0, 255],
			},
		});

		const IntensityCircle = intensify(CircleFill);

		let circle1 = new IntensityCircle(new Geometry(cartesian, [0, 0.5]), {
			intensity: 100,
			radius: 55,
		});
		let circle2 = new IntensityCircle(new Geometry(cartesian, [0.3, -0.3]), {
			intensity: 110,
			radius: 60,
		});

		let circle3 = new IntensityCircle(new Geometry(cartesian, [-0.3, -0.3]), {
			intensity: 90,
			radius: 50,
		});

		let heatpoint = new HeatPoint([1, 0], { intensity: 120, radius: 100 });

		field.multiAdd([circle1, circle2, circle3, heatpoint]);

		await platina.once("render");

		await expectPixelmatch(context, `spec/16-intensify/circles-heatmap`, 8, 0);
	});
});
