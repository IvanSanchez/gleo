# GLeo

A JS library for WebGL-powered geographic maps.

Gleo is a library for displaying geographical maps (and map-like things on cartesian planes), leveraging WebGL1 via the [Glii](https://gitlab.com/IvanSanchez/glii) abstraction library. It aims to cover the use cases of Leaflet, OpenLayers or MapboxGL/MaplibreGL, but since it's a one-man project, it's not quite there yet.

### I want to see it working!

Go to https://ivansanchez.gitlab.io/gleo/ , then.

### Trying out

1. Clone the repo
2. Run a `git submodule init` plus `git submodule update`

-   This pulls the `glii` and `arrugator` dependencies from their git repos

3. Spin up a local webserver
4. Browse the demos in `/browser-demos`, or the REPL in `/website`

No bundling needed! Gleo leverages the magic of ES6 modules.

### Documentation

Running `npm install` then `npm run docs` will build the API documentation and the graphviz UML diagram.

### Legalese

Gleo itself is licensed under a GPL-3.0 license. See the `LICENSE` file for details.

Gleo depends on some javascript libraries, which are bundled under `src/3rd-party`:

-   [`shelf-pack` by Bryan Housel](https://github.com/mapbox/shelf-pack/), ISC licensed.
-   [`css-color-parser` by Dean McNamee](https://github.com/deanm/css-color-parser-js), MIT licensed.
-   [`earcut` by Volodymir Agafonkin](https://github.com/mapbox/earcut), ISC licensed.
-   [`glmatrix` by Brandon Jones, Catto Petter, _et al_](https://github.com/toji/gl-matrix), MIT licensed.
-   [`point-geometry` by Tom McWright, _et al_](https://github.com/mapbox/point-geometry), ISC licensed.
-   [`rbush-knn` by Volodymir Agafonkin](https://github.com/mourner/rbush-knn), ISC licensed, plus dependencies:
    -   [`rbush` by Volodymir Agafonkin](https://github.com/mourner/rbush), MIT licensed.
    -   [`tinyqueue` by Volodymir Agafonkin](https://github.com/mourner/tinyqueue), ISC licensed.
    -   [`quickselect` by Volodymir Agafonkin](https://github.com/mourner/quickselect), ISC licensed.
-   [`vector-tile-js` by Volodymir Agafonkin, John Firebaugh, Tom McWright, _et al_](https://github.com/mapbox/vector-tile-js), BSD-3 licensed, plus dependencies:
    -   [`pbf` by Volodymir Agafonkin](https://github.com/mapbox/pbf), BSD-3 licensed.
    -   [`ieee754` by Feross Aboukhadijeh](https://github.com/feross/ieee754), BSD-3 licensed.
-   [`delaunator` by Volodymir Agafonkin](https://github.com/mapbox/delaunator), ISC licensed, plus dependencies:
    -   [`robust-predicates` by Volodymir Agafonkin](https://github.com/mourner/robust-predicates), on public domain dedication.

As well, Gleo uses some geographical data, assets and non-required javascript libraries in the demos:

-   [proj4js](http://proj4js.org/)
-   Natural Earth
-   [geoboundaries](https://www.geoboundaries.org/)
-   [OpenStreetMap](https://osm.org) data in several forms
-   [Leaflet.ExtraMarkers](https://github.com/coryasilva/Leaflet.ExtraMarkers) spritesheet
-   [`geographiclib`](https://geographiclib.sourceforge.io/)
-   [`geotiff.js`](https://github.com/geotiffjs/geotiff.js) by Fabian Schindler, Daniel J. Dufour, Andreas Hocevar _et al_, plus dependencies:
    -   [`xml-utils`](https://github.com/DanielJDufour/xml-utils/) by Daniel J. Dufour
    -   [`float16`](https://github.com/petamoriken/float16) by Kenta Moriuchi
    -   [`quick-lru`](https://github.com/sindresorhus/quick-lru) by Sindre Sorhus
-   [`pmtiles`](https://github.com/protomaps/pmtiles) by Brandon Liu (part of [protomaps](https://protomaps.com/))
