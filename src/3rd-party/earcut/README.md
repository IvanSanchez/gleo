This directory contains a copy of Vladimir Agafonkin's "earcut" library, trivially adapted as a ES module.

Currently at v2.2.3, as of 2021-07-12.

See https://github.com/mapbox/earcut
