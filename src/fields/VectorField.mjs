import ScalarField from "./ScalarField.mjs";

/**
 * @class VectorField
 * @inherits ScalarField
 * @relationship compositionOf Acetate, 1..1, 0..n
 *
 * Holds a two-component `float32` vector field as a platina-sized texture & framebuffer,
 * but lacks a shader program to interpret it.
 *
 * This is the abstract definition, basis for `HueVectorField`, `ArrowHeadField`, etc.
 */

export default class VectorField extends Field {
	constructor(
		target,
		{
			/**
			 * @option clearValue: Array of Number = [0, 0]
			 * The value of the scalar field prior to render data on it. It should
			 * be [zero, zero] for most cases.
			 */
			clearValue = [0, 0],

			...opts
		}
	) {
		super(target, {
			...opts,

			glFormat: 0x8227, // gl.RG
			glInternalFormat: 0x8230, // gl.RG32F
		});

		if (this.constructor.name === "VectorField") {
			throw new Error("Cannot instantiate VectorField. Use a subclass instead");
		}
	}
}
