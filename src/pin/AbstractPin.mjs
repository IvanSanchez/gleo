/**
 * @class AbstractPin
 *
 * Dummy class to optimize code usage when checking if
 * something is an `instanceof` a `HTMLPin`.
 */

export default class AbstractPin {}
