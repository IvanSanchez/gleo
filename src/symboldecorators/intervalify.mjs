/**
 * @namespace intervalify
 * @inherits Symbol Decorator
 *
 * Converts a `GleoSymbol` into a symbol that can be filtered by a value in an interval.
 *
 * Technically, this behaves as a [class mix-in](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes#mix-ins)
 * for a `GleoSymbol` and its associated `Acetate`. It's a function that
 * returns new symbol and acetate classes.
 *
 * Internally this will add two more options to the `GleoSymbol`, and two
 * properties to its `Acetate` (or rather, return a `GleoSymbol` class with two
 * more constructor options than the base `GleoSymbol` class, etc). These allow
 * for setting the start and end values of the interval that symbol should be
 * shown; in order to control what is shown, the acetate exposes two properties
 * to set the start and end values of the interval.
 *
 * @example
 *
 * A typical use case is filtering data by a time interval. This will take
 * the `Sprite` class as a base and add intervals to it.
 *
 * ```
 * import intervalify from 'gleo/src/symbols/Intervalify.mjs';
 *
 * const IntervalSprite = intervalify(Sprite);	// "Intervalified" `Sprite` class
 * ```
 *
 * Then, instantiate these `GleoSymbol`s as desired. The interval values must be
 * `Number`s, so unix timestamps are used. Beware of using
 * [`Date.parse()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/parse)
 * with anything other than [date-time strings formatted in ISO 8601](https://tc39.es/ecma262/#sec-date-time-string-format)
 * (`YYYY-MM-DD` or `YYYY-MM-DDTHH:mm:ss.sssZ`).
 *
 * ```
 * const sprite1 = new IntervalSprite(geom1, {
 * 	intervalStart: Date.parse("1950-05-30"),
 * 	intervalEnd: Date.parse("1995-11-20"),
 * 	...spriteOptions
 * };
 * const sprite2 = new IntervalSprite(geom2, {
 * 	intervalStart: Date.parse("1995-11-21"),
 * 	intervalEnd: Infinity,
 * 	...spriteOptions
 * };
 * ```
 *
 * These symbols need to be drawn in their own `Acetate` (and this `Acetate` needs
 * to be created linked to a `GleoMap` or a `Platina`):
 *
 * ```
 * const myIntervalAcetate = new IntervalSprite.Acetate(map);
 * map.addAcetate(myIntervalAcetate, 6000);
 *
 * myIntervalAcetate.multiAdd([sprite1, sprite2]);
 * ```
 *
 * Finally, set the `intervalStart` and `intervalEnd` properties of the
 * `Acetate` in order to dynamically filter which symbols are drawn:
 *
 * ```
 * myIntervalAcatate.intervalStart = Date.parse("1975-05-30");
 * myIntervalAcatate.intervalEnd = Date.parse("1975-05-30");
 * ```
 *
 * @miniclass Intervalified GleoSymbol (intervalify)
 * @section
 * The return value of the `intervalify` function is a `GleoSymbol` class. It
 * will behave as the base class used, but with two extra constructor options:
 *
 * @option intervalStart: Number
 * The start (minimum value) of the symbol's numeric interval.
 * @option intervalEnd: Number
 * The end (maximum value) of the symbol's numeric interval.
 *
 * @miniclass Intervalified Acetate (intervalify)
 * @section
 *
 * Interval-enabled symbols need their own acetate to be drawn into - the
 * example code above shows how to access it from an intervalified symbol
 * class, and how to instantiate and add it to a map/platina.
 *
 * An intervalified `Acetate` gains two new read/write properties. Setting
 * a new value to any of these will trigger a re-render.
 *
 * @property intervalStart: Number
 * The start (minimum value) of the symbol's numeric interval. Any symbols
 * in this acetate with an interval that falls outside the acetate's own
 * interval will **not** be drawn.
 * @property intervalEnd: Number
 * The end (maximum value) of the symbol's numeric interval.
 *
 * @namespace intervalify
 * @function intervalify(base: GleoSymbol): GleoSymbol
 *
 * "Intervalifies" the given `GleoSymbol` class, i.e. returns a new `GleoSymbol`
 * class that behaves like the original, but can contain information about a
 * numeric interval. The corresponing `Acetate` also defines a numeric interval,
 * and will filter out symbols outside whose interval is outside of the acetate's
 * own interval.
 *
 */

export default function intervalify(base) {
	class IntervalifiedAcetate extends base.Acetate {
		constructor(
			target,
			{ intervalStart = -Infinity, intervalEnd = Infinity, ...opts } = {}
		) {
			super(target, opts);

			this._intervalAttr = new this.glii.SingleAttribute({
				usage: this.glii.STATIC_DRAW,
				size: 1,
				growFactor: 1.2,

				// Start value in `.x`, end value in `.y`
				glslType: "vec2",
				type: Float32Array,
				normalized: false,
			});

			this.intervalStart = intervalStart;
			this.intervalEnd = intervalEnd;

			// // Ideally, this mix-in would redefine `multiAdd`, and inside
			// // there call `super.multiAdd(symbols)`. But, since symbol allocation
			// // can be async (e.g. in `Sprite`s), the reliable way to fetch
			// // allocated symbols is to listen to the "symbolsadded" event.
			// this.on("symbolsadded", this.#onSymbolsAdded.bind(this));
		}

		glProgramDefinition() {
			const opts = super.glProgramDefinition();

			return {
				...opts,
				attributes: {
					aInterval: this._intervalAttr,
					...opts.attributes,
				},
				uniforms: {
					uInterval: "vec2",
					...opts.uniforms,
				},
				vertexShaderMain: `
				if (aInterval.x > uInterval.x && aInterval.y < uInterval.y) {
					${opts.vertexShaderMain}
				}
				`,
			};
		}

		// #onSymbolsAdded(ev) {
		// 	const symbols = ev.detail.symbols;
		//
		// 	// This makes the ASSUMPTION that the parent functionality kept the
		// 	// order of the symbols intact, and that the attribute allocation
		// 	// runs from the `attrBase` of the first symbol to the
		// 	// `attrBase`+`attrLength` of the last symbol.
		//
		// 	const attrBase = symbols[0].attrBase;
		//
		// 	const intervals = symbols
		// 		.map((s) =>
		// 			new Array(s.attrLength).fill([s.intervalStart, s.intervalEnd])
		// 		)
		// 		.flat(2);
		//
		// 	this._intervalAttr.multiSet(attrBase, intervals);
		//
		// 	return this;
		// }

		_getStridedArrays(maxVtx, maxIdx) {
			return [
				// min/max interval values for vertex
				this._intervalAttr.asStridedArray(maxVtx),
				// Parent strided arrays
				...super._getStridedArrays(maxVtx, maxIdx),
			];
		}

		_commitStridedArrays(baseVtx, vtxCount) {
			this._intervalAttr.commit(baseVtx, vtxCount);
			return super._commitStridedArrays(baseVtx, vtxCount);
		}

		#intervalStart;
		#intervalEnd;
		#dirtyIntervals = false;
		get intervalStart() {
			return this.#intervalStart;
		}
		get intervalEnd() {
			return this.#intervalEnd;
		}
		set intervalStart(i) {
			this.#intervalStart = i;
			this.dirty = this.#dirtyIntervals = true;
		}
		set intervalEnd(i) {
			this.#intervalEnd = i;
			this.dirty = this.#dirtyIntervals = true;
		}
		redraw() {
			if (this.#dirtyIntervals) {
				this._programs.setUniform("uInterval", [
					this.#intervalStart,
					this.#intervalEnd,
				]);
				this.#dirtyIntervals = false;
			}
			return super.redraw.apply(this, arguments);
		}
	}

	class IntervalifiedSymbol extends base {
		static Acetate = IntervalifiedAcetate;

		constructor(
			geom,
			{ intervalStart = undefined, intervalEnd = undefined, ...opts } = {}
		) {
			super(geom, opts);
			this.intervalStart = intervalStart;
			this.intervalEnd = intervalEnd;
		}

		_setGlobalStrides(strideInterval, ...strides) {
			for (let i = this.attrBase, t = this.attrBase + this.attrLength; i < t; i++) {
				strideInterval.set([this.intervalStart, this.intervalEnd], i);
			}

			return super._setGlobalStrides(...strides);
		}
	}

	return IntervalifiedSymbol;
}
