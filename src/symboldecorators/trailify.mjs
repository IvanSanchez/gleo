/**
 * @namespace trailify
 * @inherits Symbol Decorator
 *
 * Applies only to line symbols (either `Hair` or `Stroke`). The symbol gains
 * a m-value option, like in `trajectorify`. The acetate can then set a lower
 * and upper threshold for that m-value.
 *
 * The symbol will be drawn only when the (interpolated) m-value of a pixel
 * falls within the two thresholds of the acetate. The opacity of each pixel
 * depends on how close the m-value is to the upper threshold.
 */

export default function trailify(base) {
	let valid = false;
	let proto = base;
	while (!valid) {
		if (!proto) {
			throw new Error(
				"The 'trailify' symbol decorator can only be applied to Stroke, Chain, or Hair"
			);
		}
		valid |=
			proto.name === "Stroke" || proto.name === "Chain" || proto.name === "Hair";
		proto = proto.__proto__;
	}

	// By default, the trail will fade out on the alpha component...
	let component = "a";
	if (base.Acetate.PostAcetate?.name === "ScalarField") {
		// Except if the symbol is a HeatChain/HeatStroke/etc; in this case,
		// fade out the red (first) channel
		component = "r";
	}

	class TrailifiedAcetate extends base.Acetate {
		#minMValue;
		#maxMValue;

		constructor(target, opts = {}) {
			super(target, opts);

			// M-coordinates, per vertex.
			this._mcoords = new this.glii.SingleAttribute({
				size: 1,
				growFactor: 1.2,
				usage: this.glii.DYNAMIC_DRAW,
				glslType: "float",
				type: Float32Array,
			});
		}

		glProgramDefinition() {
			const opts = super.glProgramDefinition();

			return {
				...opts,
				attributes: {
					...opts.attributes,
					aMCoord: this._mcoords,
				},
				uniforms: {
					...opts.uniforms,
					uMThreshold: "vec2", // Lower threshold, higher-lower delta
				},
				varyings: {
					...opts.varyings,
					vMCoord: "float",
				},
				vertexShaderMain: `${opts.vertexShaderMain}
					vMCoord = aMCoord;
				`,

				fragmentShaderMain: `${opts.fragmentShaderMain}
					if (vMCoord > (uMThreshold.x + uMThreshold.y)) {
						discard;
					} else {
						gl_FragColor.${component} *= max(0.0,
							(vMCoord - uMThreshold.x) / uMThreshold.y
						);
					}
				`,
			};
		}

		/**
		 * @property minMValue
		 * The lower threshold for the m-values of symbols. Can be updated.
		 * @property maxMValue
		 * The higher threshold for the m-values of symbols. Can be updated.
		 */
		get minMValue() {
			return this.#minMValue;
		}
		get maxMValue() {
			return this.#maxMValue;
		}
		set minMValue(m) {
			this.#minMValue = m;
			this.#updateMValues();
		}
		set maxMValue(m) {
			this.#maxMValue = m;
			this.#updateMValues();
		}

		#updateMValues() {
			this._programs.setUniform("uMThreshold", [
				this.#minMValue,
				this.#maxMValue - this.#minMValue,
			]);
			this.dirty = true;
		}

		_getPerPointStridedArrays(maxVtx, maxIdx) {
			return [
				// M-value
				this._mcoords.asStridedArray(maxVtx),

				// Parent strided arrays
				...super._getPerPointStridedArrays(maxVtx, maxIdx),
			];
		}

		_commitPerPointStridedArrays(baseVtx, vtxCount) {
			this._mcoords.commit(baseVtx, vtxCount);
			return super._commitPerPointStridedArrays(baseVtx, vtxCount);
		}
	}

	/**
	 * @miniclass Trailified GleoSymbol (trailify)
	 *
	 * A "trailified" symbol accepts this additional constructor options:
	 */
	return class trailifiedSymbol extends base {
		static Acetate = TrailifiedAcetate;

		#mCoords;

		constructor(
			geom,
			{
				/**
				 * @option mcoords: Array of Number = []
				 *
				 * The values for the M-coordinates of the geometry. This **must** have one
				 * M-coordinate per point in the symbol's geometry.
				 *
				 */
				mcoords = [],

				...opts
			} = {}
		) {
			super(geom, opts);
			this.#mCoords = mcoords;
		}

		_setPerPointStrides(n, pointType, vtx, vtxCount, strideMcoords, ...strides) {
			super._setPerPointStrides(n, pointType, vtx, vtxCount, ...strides);

			for (let i = 0; i < vtxCount; i++) {
				strideMcoords.set([this.#mCoords[n]], vtx + i);
			}
		}
	};
}
